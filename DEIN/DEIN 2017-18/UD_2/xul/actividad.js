function calcular()
{
	var dateString = document.getElementById("fecha").value;
	var date = new Date(dateString);
	
	//para que coja los a�os anteriores al a�o 1000
	var arrFecha = dateString.split("-");
	date.setUTCFullYear(arrFecha[0]);
	
	var day = date.getDay();
	console.log(day);
	//resetear dias
	var dias = document.getElementsByClassName("dia");
	for (var i = 0; i < dias.length; i++) {
		dias[i].className = "dia";
	}
	
	//printear el dia seleccionado en negro
	document.getElementById("d"+day).className += " diaActivo";
}

function hoy()
{
	var date = new Date();
	var day = date.getDate();
	var month = date.getMonth() + 1;
	var year = date. getYear() + 1900;

	document.getElementById("fecha").value = year + "-" + month + "-" + day;
	
	calcular();
}

function clear()
{
	document.getElementById("fecha").value="";
	var dias = document.getElementsByClassName("dia");
	for (var i = 0; i < dias.length; i++) {
		dias[i].className = "dia";
	}
}