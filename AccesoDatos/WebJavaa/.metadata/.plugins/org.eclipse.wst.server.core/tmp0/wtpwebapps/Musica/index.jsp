<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	
	<link rel="stylesheet" href="estilos.css">
	
	<title>Artistas/Discos</title>
</head>
<body>
	<div>
		<h2>Artistas / Discos</h2>
	
		<table border="1px solid black">
        <tr>
            <td><a href="#">Nuevo disco</a></td>
        </tr>
        <tr>
            <td><a href="#">Nuevo artista</a></td>
        </tr>
        <tr>
            <td><a href="#">Mostrar</a></td>
        </tr>
        <tr>
            <td><a href="#">Eliminar</a></td>
        </tr>
    </table>
	</div>
</body>
</html>