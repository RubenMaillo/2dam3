package com.example.navegacion;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Secundaria extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secundaria);

        TextView texto1 = (TextView) findViewById(R.id.texto);

        //String valor = getIntent().getStringExtra("usuario");

        texto1.setText("Hola " + getIntent().getStringExtra("nombre") + "!");
    }
}
