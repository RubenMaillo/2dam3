/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { View,  FlatList, Text, StyleSheet, ToastAndroid} from 'react-native';
import Sound from 'react-native-sound';

class App extends Component {
  
  render(){

    const animales = [
      {
        id:1,
        title: 'Perro',
        sonido: "Guau",
        url: "./audio/008699955_prev.mp3"
      },
      {
        id:2,
        title: 'Gato',
        sonido: "Miau",
        url: "./audio/008700019_prev.mp3"
      },
      {
        id:3,
        title: 'Gallo',
        sonido: "Quiquiriqui",
        url: "./audio/Gallo.mp3"
      },
      {
        id:4,
        title: 'Burro',
        sonido: "Ih-oh",
        url: "./audio/animals020.mp3"
      }
    ]

    function Item({ title, sonido, url }) {
      return (
        <View style={styles.item}>
          <Text style={styles.title} onPress={function(){
            ToastAndroid.show(sonido, ToastAndroid.SHORT);
          }}>{title}</Text>
        </View>
      );
    }

    return(
      <View style={styles.contenedor}>
        <FlatList style={styles.lista}
          data={animales}
          renderItem={({item}) => <Item title={item.title} sonido={item.sonido} url={item.url}/>}
          keyExtractor={item => item.id}
        />
      </View>
    );
  };
  
};

const styles = StyleSheet.create({
  contenedor:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },

  item:{
    padding: 15,
  },
  
  title:{
    fontSize: 18,
  }
})

export default App;
