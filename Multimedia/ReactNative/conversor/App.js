/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TextInput,
  Button
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

class App extends Component{

  state = { nudos:'', kilometros: '' }

  nudosAKm(){
    var nudosV, kilometrosV;
    nudosV = parseFloat(this.state.nudos);
    kilometrosV = nudosV * 1.852;
    this.setState({kilometros: kilometrosV.toString()})
  }

  kmANudos(){
    var nudosV, kilometrosV;
    kilometrosV = parseFloat(this.state.kilometros);
    nudosV = kilometrosV / 1.852;
    this.setState({nudos: nudosV.toString()})
  }

  render(){
    return(
      <View style={styles.contenedor}>
        <View style={styles.contenedor2}>
          <TextInput placeholder="Nudos" value = {this.state.nudos} onChangeText = {text => this.setState({ nudos: text})}/>
          <TextInput placeholder="Km/h" value = {this.state.kilometros} onChangeText = {text => this.setState({ kilometros: text})}/>
          <Button title={"Nudos a Km/h"} onPress={() => this.nudosAKm()}/>
          <Button title={"Km a Nudos"} onPress={() => this.kmANudos()}/>
        
        </View>
      </View>
    );
  };
}

const styles = StyleSheet.create({
  contenedor:{
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  contenedor2:{
    flex: 0.4,
    justifyContent: 'space-around',
    alignItems: 'center'
  }
  
});

export default App;
