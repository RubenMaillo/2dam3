import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Index from './vistas/componentes/Home';
import Segunda from './vistas/componentes/Segunda';

const RootStack = createStackNavigator({
  home: {
    screen: Index,
    header: null,
  },
  segunda: {
    screen: Segunda,
    header: null,
  }
});

const App = createAppContainer(RootStack);

export default App;