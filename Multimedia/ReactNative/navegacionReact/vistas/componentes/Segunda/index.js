import React, { Component } from 'react'
import { Text, View, Button, StyleSheet } from 'react-native';


class Home extends Component{
    static navigationOptions = {
        header: null,
        numBoton: ""
    }

    navegar = (title) => {
        // console.log("tilte:"+title)
        this.props.navigation.navigate('home', { numBoton: title })
    }

    render(){
        return(
            <View style={styles.contenedor}>
                <Text>Hola {this.props.navigation.state.params.nombre}</Text>
                <View style={styles.botones}>
                    <Button title={"Volver 1"} color='#366482' onPress={() => this.navegar("1")}/>
                    <Button title={"Volver 2"} color='#366482' onPress={() => this.navegar("2")}/>
                </View>
            </View>
        )
    };
}

export default Home;

const styles = StyleSheet.create({
    contenedor:{
        flex: 0.5,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    botones: {
        width: "60%",
        flexDirection: "row",
        justifyContent: "space-around",
        paddingBottom: 20
    }
});