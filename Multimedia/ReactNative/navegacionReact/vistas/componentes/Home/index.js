import React, { Component } from 'react'
import { Text, View, Button, StyleSheet, TextInput } from 'react-native';

class Home extends Component{
    static navigationOptions = {
        header: null
    }

    constructor(props){
        super(props)
        this.state={
            nombre:'',
        }
    }

    navegar(){
       this.props.navigation.navigate('segunda', { nombre: this.state.nombre })

    }

    render(){
        
        return(
            <View style={styles.contenedor}>
                <TextInput 
                    style={styles.input}
                    placeHolder={"Nombre"}
                    value={this.state.nombre}
                    onChangeText={nombre => this.setState({nombre: nombre})}
                />
                <Button title={"Navegar"} color='#366482' onPress={() => this.navegar()} />

                <Text>Ha pulsado el boton {this.props.navigation.getParam('numBoton')}</Text>
            </View>
        )
    };
}

export default Home;

const styles = StyleSheet.create({
    contenedor:{
        flex: 0.5,
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    input: {
        width: "40%",
        borderBottomWidth: 1,
        padding: 5
    }
});