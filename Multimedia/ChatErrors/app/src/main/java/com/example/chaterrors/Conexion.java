package com.example.chaterrors;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Conexion extends AsyncTask <Void, Void, String> {

    Socket socket = null;
    //String ipServer = "192.168.0.14";
    String ipServer = "192.168.137.1";
    int port = 3333;

    static HandlerThread mainThread;
    static Handler mainHandler;

    static OutputStream os;

    static HandlerThread envioThread;
    static Handler envio;

    static HandlerThread escuchaThread;
    static Handler escucha;

    BufferedReader entrada;
    BufferedWriter salida;

    String s;

    protected String doInBackground(Void... voids) {

        try {

            socket = new Socket(ipServer, port);
            entrada = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            os = socket.getOutputStream();
            Log.e("CONEXION", entrada.readLine());

            //envio de datos
            envioThread = new HandlerThread("Envio");
            envioThread.start();
            envio = new Handler(envioThread.getLooper());

            //recibir datos
            escuchaThread = new HandlerThread("Escucha");
            escuchaThread.start();
            escucha = new Handler(escuchaThread.getLooper());


            escucha.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        while ((s = entrada.readLine()) != null){
                            MainActivity.mainThread.post(new Runnable() {
                                @Override
                                public void run() {
                                    Message msg = MainActivity.mainThread.obtainMessage();
                                    Bundle bundle = new Bundle();
                                    bundle.putString("Mensaje", s);
                                    msg.setData(bundle);
                                    MainActivity.mainThread.sendMessage(msg);
                                }
                            });
                        }
                    }
                    catch (Exception e) {
                        Log.e("ERROR", e.toString());
                    }
                }
            });

        } catch (Exception e) {

            Log.e("ERROR", e.toString());

        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
       //Log.e("Resultado", s);
    }

    public static void enviar(final String mensaje, final TextView campoMensaje){
        envio.post(new Runnable() {
            @Override
            public void run() {

            if(os != null){
                try {
                    String s = mensaje + "\n";
                    os.write(s.getBytes());
                    os.flush();
                }
                catch (IOException e){
                    Log.e("ERROR", e.toString());
                }
            }

            }
        });
    }

    public void escribir(String s){
        Log.e("RETURN", s);
    }

}
