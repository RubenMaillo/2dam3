
public class Triangulo extends FiguraGeometrica{

	private double l1, l2, l3;
	
	public Triangulo(int x, int y, double l1, double l2, double l3) {
		super(x, y);
		this.l1 = l1;
		this.l2 = l2;
		this.l3 = l3;
	}

	public double getL1() {
		return l1;
	}

	public void setL1(double l1) {
		this.l1 = l1;
	}

	public double getL2() {
		return l2;
	}

	public void setL2(double l2) {
		this.l2 = l2;
	}

	public double getL3() {
		return l3;
	}

	public void setL3(double l3) {
		this.l3 = l3;
	}

	@Override
	public double area() {
		double semipem;
		
		semipem = (l1+l2+l3)/2;
		
		return Math.sqrt(semipem*(semipem-l1)*(semipem-l2)*(semipem-l3));
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return l1+l2+l3;
	}
	
	public boolean esIsosceles() {
		if (l1==l2 || l1==l3 || l2==l3)
	        return true;
	    else
	    	return false;
	}
	
	public boolean esEquilatero() {
		if (l1==l2 && l1==l3)
	        return true;
	    else
	    	return false;
	}
	
	public boolean esEscaleno() {
		if (l1!=l2 || l1!=l3 || l3!=l2)
	        return true;
	    else
	    	return false;
	}
}
