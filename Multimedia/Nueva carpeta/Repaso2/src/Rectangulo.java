
public class Rectangulo extends FiguraGeometrica{

	private double base;
	private double altura;
	
	public Rectangulo(int x, int y, double base, double altura) {
		super(x, y);
		this.base = base;
		this.altura = altura;
	}

	public double getBase() {
		return base;
	}

	public void setBase(float base) {
		this.base = base;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(float altura) {
		this.altura = altura;
	}

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return base*altura;
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return 2*base+2*altura;
	}
	
}
