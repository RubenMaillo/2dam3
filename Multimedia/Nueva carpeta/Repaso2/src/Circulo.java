
public class Circulo extends FiguraGeometrica{
	
	private double radio;

	public Circulo(int x, int y, double radio) {
		super(x, y);
		this.radio = radio;
	}

	public double getRadio() {
		return radio;
	}

	public void setRadio(float radio) {
		this.radio = radio;
	}	

	@Override
	public double area() {
		// TODO Auto-generated method stub
		return Math.PI*radio*radio;
	}

	@Override
	public double perimetro() {
		// TODO Auto-generated method stub
		return 2.0*Math.PI*radio;
	}	
	
}
