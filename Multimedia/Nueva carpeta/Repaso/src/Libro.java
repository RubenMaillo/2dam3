
public class Libro extends Ejemplar implements Imprimible{
	
	private String autor;
	private String editorial;

	public Libro(int id, String titulo, String autor, String editorial) {
		super(id, titulo);
		
		this.autor = autor;
		this.editorial = editorial;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getEditorial() {
		return editorial;
	}

	public void setEditorial(String editorial) {
		this.editorial = editorial;
	}
	
	@Override
	public void imprimir() {
		super.imprimir();
		System.out.println("Autor-->" + getAutor());
		System.out.println("Editorial--> " + getEditorial() + "\n");
	}
	
	@Override
	public void reproducir() {
		System.out.println("------------------");
		System.out.println("|   Leyendo...   |");
		System.out.println("------------------\n");
	}

	@Override
	public void estampar() {
		System.out.println("ииииииииииииииииииииии");
		System.out.println("|   Imprimiendo...   |");
		System.out.println("ииииииииииииииииииииии\n");
	}
}
