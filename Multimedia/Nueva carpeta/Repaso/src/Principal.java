import java.io.IOException;
import java.util.ArrayList;

public class Principal {

	public static void main (String[] args) {
		
		ArrayList<Ejemplar> lista = new ArrayList<Ejemplar>();
		
		/*for(int i = 0; i <20; i++) {
			lista.add(new Ejemplar(i, "Libro"+i));
		}
		
		for(int i = 0; i < lista.size(); i++) {
			lista.get(i).imprimir();
		}
		
		Otra forma de recorrer un ArrayList
		 * 
		 * for(Ejemplar iej : lista) {
				iej.imprimir();
		}
		
		Libro l = new Libro(1, "asd", "asd", "asd");
		l.imprimir();
		
		Documental d = new Documental(1, "fgh", "fgh", "fgh", "fgh");
		d.imprimir();*/
		
		for(int i = 0; i <20; i++) {
			lista.add(new Libro(i+1, "El barco", "Pepe", "Magdalena"));
			lista.add(new Documental(i+1, "fgh", "fgh", "fgh", "fgh"));
		}
		
		//Ejemplar ej = lista.get(0);
		
		//System.out.println(((Libro) ej).getAutor());
		
		for(Ejemplar ej : lista) {
			ej.imprimir();
			
			//if(ej instanceof Libro) {
			
			
			ej.reproducir();
			
			//}
		}
		
	}
	
}
