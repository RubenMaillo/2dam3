import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class ConCliente extends Thread{
	
	Socket con;
	
	public ConCliente(Socket con) {
		this.con = con;
	}

	public void run() {
		
		try {
			
			System.out.println("Cliente conectado --> " + con.getRemoteSocketAddress().toString());
			
			BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
			bw1.write("Conectado\n");
			bw1.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String linea = null;
			linea = br.readLine();
			
		
			while(!linea.equalsIgnoreCase("END")) {
				broadcast(con, linea);
				linea = br.readLine();
			}
			
			cerrarConexion(con);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void broadcast(Socket con, String linea) {
		System.out.println(Servidor.clientes.size());
		try {
			for(Socket sock : Servidor.clientes) {
				if(!(sock.getRemoteSocketAddress().toString()).equalsIgnoreCase(con.getRemoteSocketAddress().toString())) {
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
					System.out.println("Cliente " + sock.getRemoteSocketAddress().toString() + ": "+linea);
					bw.write(sock.getRemoteSocketAddress().toString() + ": " + linea +"\n");
					bw.flush();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void cerrarConexion(Socket con) {
		try {
			con.close();
			Servidor.clientes.remove(con);
			System.out.println("Conexi�n cerrada con el cliente " + con.getRemoteSocketAddress().toString() + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}
