import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor{

	
	public static void main(String[] args) {
		iniciar();
	}
	
	public static void iniciar() {
		
		try {
			ServerSocket server = new ServerSocket(3333);
			
		
			while(true) {
				Socket client = server.accept();
				System.out.println("Alguien se ha conectado");
				
				//new Cliente().start();
				
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
				
				bw.write("Hola\n");
				
				bw.flush();
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error!");
		}
	}
}
