import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente /*extends Thread*/{
	
	public static void main(String[] args) {
		iniciar();
	}
	
	public static void iniciar() {
		
		Socket s;
		BufferedReader bf;
		
		try {
			
			s = new Socket("localhost", 3333);
			bf = new BufferedReader(new InputStreamReader(s.getInputStream()));
			String linea = null;
			
			while((linea = bf.readLine()) != null) {
				System.out.println("Cliente: " + linea);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/*public void run() {
		System.out.println("Hola soy un hilo");
	}*/
	
}
