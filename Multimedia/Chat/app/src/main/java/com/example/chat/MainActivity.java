package com.example.chat;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class MainActivity extends AppCompatActivity {

    static TextView mensajes;
    static Handler mainThread;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mensajes = findViewById(R.id.mensajes);
        mainThread = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                String mensaje = bundle.getString("Mensaje");
                mensajes.append("\n" + mensaje);
            }
        };

        final Conexion conexion = new Conexion();
        conexion.execute();

        final EditText mensaje = findViewById(R.id.mensaje);

        FloatingActionButton send = findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                mensajes.append("\nYo:" + mensaje.getText().toString());
                conexion.enviar(mensaje.getText().toString(), mensajes);
                mensaje.setText("");
            }
        });
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        mensajes.setText(savedInstanceState.getString("Mensajes"));
    }

    // invoked when the activity may be temporarily destroyed, save the instance state here
    @Override
    public void onSaveInstanceState(Bundle outState) {
        Conexion.cerrar();
        Conexion.envioThread.quit();
        Conexion.escuchaThread.quit();
        outState.putString("Mensajes", mensajes.getText().toString());
        // call superclass to save any view hierarchy
        super.onSaveInstanceState(outState);
    }
}
