package com.example.serviceprueba;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

public class Conexion extends AsyncTask<Void,Void,Void> {

    Socket socket ;
    BufferedWriter bw;
    BufferedReader br;

    //Messenger hHiloPrincipal;
    Handler hHiloPrincipal; // handler service

    Handler hEscucha;
    HandlerThread htEscucha;

    Handler hEnvio;
    HandlerThread htEnvio;

    Message message;


    public Conexion (Handler handler) {
        hHiloPrincipal = handler;
    }


    @Override
    protected Void doInBackground(Void... voids) {

        try {
            socket = new Socket("192.168.0.18", 3333);
            Log.e("conexion","ddddddddddddddddddddddddddddddddd");
            bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        htEscucha = new HandlerThread("htEScucha");
        htEscucha.start();
        hEscucha = new Handler(htEscucha.getLooper());

        htEnvio = new HandlerThread("htEnvio");
        htEnvio.start();
        hEnvio = new Handler(htEnvio.getLooper());

        hEscucha.post(new Runnable() {
            @Override
            public void run() {
                String msg;
                try {

                    while ((msg = br.readLine()) != null) {
                        message = hHiloPrincipal.obtainMessage();
                        Bundle bundle = new Bundle();
                        bundle.putString("MSG",msg);
                        message.setData(bundle);
                        hHiloPrincipal.sendMessage(message);

                    }
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        return null;
    }

    public void enviarMensajeAlServer(String s) {
        final String msg = s;
        hEnvio.post(new Runnable() {
            @Override
            public void run() {
                try {
                    bw.write(msg+"\n");
                    bw.flush();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void desconectar() {
        try {

            hEnvio.removeCallbacksAndMessages(null);
            hEnvio.getLooper().quit();

            hEscucha.removeCallbacksAndMessages(null);
            hEscucha.getLooper().quit();

            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
