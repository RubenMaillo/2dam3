package com.example.serviceprueba;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import java.util.ArrayList;


public class MyService extends Service {

    private NotificationManager nm;
    private static boolean isRunning = false;

    // Supone que puede haber más de un activity conectado al service. Tendremos solamete uno
    ArrayList<Messenger> mClients = new ArrayList<Messenger>(); // Keeps track of all current registered clients.

    // codigos para mensajes especiales
    static final int MSG_REGISTER_CLIENT = 1;
    static final int MSG_UNREGISTER_CLIENT = 2;
    //static final int MSG_SET_INT_VALUE = 3;
    //static final int MSG_SET_STRING_VALUE = 4;

    final Messenger mMessenger = new Messenger(new IncomingHandler()); // Target we publish for clients to send messages to IncomingHandler.

    // Clase conexion para realizar en un hilo aparte las operaciones de red
    Conexion conexion;

    @Override
    public IBinder onBind(Intent intent) {
        return mMessenger.getBinder();
    }

    class IncomingHandler extends Handler { // Handler of incoming messages from clients.
        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case MSG_REGISTER_CLIENT:
                    mClients.add(msg.replyTo);
                    break;
                case MSG_UNREGISTER_CLIENT:
                    mClients.remove(msg.replyTo);
                    break;
                default:
                    conexion.enviarMensajeAlServer(msg.getData().getString("MSG").toString());
                    //super.handleMessage(msg);
            }
        }
    }

    private void sendMessageToUI(String mensaje) {
        for (int i=mClients.size()-1; i>=0; i--) {
            // Bucle de un solo cliente en nuestro caso
            try {

                //Send data as a String
                Bundle b = new Bundle();
                b.putString("MSG", mensaje);
                Message msg = Message.obtain();
                msg.setData(b);
                mClients.get(i).send(msg);

            }
            catch (RemoteException e) {
                // The client is dead. Remove it from the list; we are going through the list from back to front so this is safe to do inside the loop.
                mClients.remove(i);
            }
        }
    }

    //
    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("MyService", "Service Started.");
        showNotification();

        isRunning = true;

        // Creamos la clase conexion
        conexion = new Conexion(new Handler() {
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                sendMessageToUI(msg.getData().getString("MSG"));
            }
        });
        conexion.execute();
    }

    private static final int NOTIFICATION_ID = 45349;
    private void showNotification() {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("My Notification Title")
                        .setContentText("Something interesting happened");

        Intent targetIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, targetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);
        nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(NOTIFICATION_ID, builder.build());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        nm.cancel(NOTIFICATION_ID); // Cancel the persistent notification.
        Log.i("PlaybackService", "Service Stopped.");
        isRunning = false;

        // matar conexion al server de chat
        conexion.desconectar();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("MyService", "Received start id " + startId + ": " + intent);
        return START_STICKY; // run until explicitly stopped.
    }

    public static boolean isRunning() {
        return isRunning;
    }
}