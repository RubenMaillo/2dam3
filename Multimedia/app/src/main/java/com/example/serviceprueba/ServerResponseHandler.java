package com.example.serviceprueba;

import android.os.Message;

public interface ServerResponseHandler {


    public void success(Message[] msgs); // msgs may be null if no new messages
    public void error();

}
