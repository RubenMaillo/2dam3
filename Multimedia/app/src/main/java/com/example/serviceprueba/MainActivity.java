package com.example.serviceprueba;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {


    // basado en https://developer.android.com/reference/android/app/Service.html#remote-messenger-service-sample
    // https://stackoverflow.com/questions/4300291/example-communication-between-activity-and-service-using-messaging?rq=1


    Messenger mService = null;
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound = false;
    Conexion conexion;
    Button bEnvio;

    TextView tv;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            /*switch (msg.what) {
                case MyService.MSG_SET_INT_VALUE:
                    textIntValue.setText("Int Message: " + msg.arg1);
                    break;
                case MyService.MSG_SET_STRING_VALUE:
                    String str1 = msg.getData().getString("str1");
                    textStrValue.setText("Str Message: " + str1);
                    break;
                default:
                    super.handleMessage(msg);
            }*/
            tv.append(msg.getData().getString("MSG")+"\n");
        }
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tv = findViewById(R.id.tvTexto);

        // se conecta al servicio (da igual que esté ya iniciado o no)
        doBindService();

        final EditText etEnvio = findViewById(R.id.etEnvio);

        bEnvio = findViewById(R.id.bEnvio);
        bEnvio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // (version sin service)
                //conexion.enviarMensajeAlServer(etEnvio.getText().toString());

                // version con service
                sendMessageToService(etEnvio.getText().toString());
            }
        });

    }

    void doBindService() {
        Intent i = new Intent(this, MyService.class);
        startService(i);
        bindService(i, mConnection, Context.BIND_AUTO_CREATE);
        mIsBound = true;
        //textStatus.setText("Binding.");
    }


    private ServiceConnection mConnection = new ServiceConnection() {
        // se invoca cuando nos conectamos al service
        public void onServiceConnected(ComponentName className, IBinder service) {
            // se obtiene Messenger para comunicarse con el server
            mService = new Messenger(service);
            Log.e("MSG","Servicio conectado!");

            try {
                // se envía mensaje con código para que el service registre el cliente
                Message msg = Message.obtain(null, MyService.MSG_REGISTER_CLIENT);
                msg.replyTo = mMessenger;
                mService.send(msg);
            }
            catch (RemoteException e) {
                // In this case the service has crashed before we could even do anything with it
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            // This is called when the connection with the service has been unexpectedly disconnected - process crashed.
            mService = null;
            //textStatus.setText("Disconnected.");
        }
    };

    // no se usa, porque el bind inicia el service
    private void CheckIfServiceIsRunning() {
        //If the service is running when the activity starts, we want to automatically bind to it.
        if (MyService.isRunning()) {
            doBindService();
        }
    }


    private void sendMessageToService(String mensaje) { //param. original intvaluetosend
        if (mIsBound) {
            if (mService != null) {
                try {
                    //Message msg = Message.obtain(null, MyService.MSG_SET_INT_VALUE, intvaluetosend, 0);
                    Message msg = Message.obtain();
                    msg.replyTo = mMessenger;
                    Bundle bundle = new Bundle();
                    bundle.putString("MSG",mensaje);
                    msg.setData(bundle);
                    mService.send(msg);
                }
                catch (RemoteException e) {
                }
            }
        }
    }

    // desconectar cliente del service enviando msg con código especial
    void doUnbindService() {
        if (mIsBound) {
            // If we have received the service, and hence registered with it, then now is the time to unregister.
            if (mService != null) {
                try {
                    Message msg = Message.obtain(null, MyService.MSG_UNREGISTER_CLIENT);
                    msg.replyTo = mMessenger;
                    mService.send(msg);
                }
                catch (RemoteException e) {
                    // There is nothing special we need to do if the service has crashed.
                }
            }
            // Detach our existing connection.
            unbindService(mConnection);
            mIsBound = false;
            //textStatus.setText("Unbinding.");
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            doUnbindService();
        }
        catch (Throwable t) {
            Log.e("MainActivity", "Failed to unbind from the service", t);
        }
    }


}
