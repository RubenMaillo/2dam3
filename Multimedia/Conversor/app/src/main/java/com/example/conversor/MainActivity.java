package com.example.conversor;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button bNudosKm = (Button) findViewById(R.id.NudosAKm);
        Button bKmNudos = (Button) findViewById(R.id.KmANudos);

        bNudosKm.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditText eNudos= (EditText) findViewById(R.id.Nudos);
                EditText eKm= (EditText) findViewById(R.id.Kilometros);

                String nudos = eNudos.getText().toString();

                Double fNudos = Double.parseDouble(nudos);
                Double fKm = fNudos * 1.852;
                eKm.setText(fKm.toString());
            }
        });

        bKmNudos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                EditText eNudos= (EditText) findViewById(R.id.Nudos);
                EditText eKm= (EditText) findViewById(R.id.Kilometros);

                String kilometros = eKm.getText().toString();

                Double fKm = Double.parseDouble(kilometros);
                Double fNudos = fKm * 0.54;
                eNudos.setText(fNudos.toString());
            }
        });

    }


}
