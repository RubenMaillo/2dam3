package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.widget.Switch;
import android.widget.CompoundButton;
import android.view.View;


public class MainActivity extends AppCompatActivity {
    MediaPlayer cancion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Switch bucle = (Switch)  findViewById(R.id.bucle);
        bucle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    cancion.setLooping(true);
                else{
                    cancion.setLooping(false);
                }
            }

        });
    }

    public void play(View v){
        if(cancion == null){
            cancion = MediaPlayer.create(this, R.raw.pajaro);
        }
        cancion.start();
    }

    public void pause(View v){
        if(cancion != null){
            cancion.pause();
        }
    }
}
