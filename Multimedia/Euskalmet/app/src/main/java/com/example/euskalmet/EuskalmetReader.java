package com.example.euskalmet;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class EuskalmetReader extends AsyncTask<String, Void, String> {

    XmlPullParser xpp;
    String prevision;
    protected String doInBackground(String... s) {
        HttpURLConnection urlConnection = null;
        try {

            URL url = new URL("http://www.euskalmet.euskadi.eus/contenidos/prevision_tiempo/met_forecast/opendata/met_forecast.xml");
            urlConnection= (HttpURLConnection) url.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());


            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                xpp = factory.newPullParser();

                xpp.setInput( new InputStreamReader(in, "ISO-8859-1"));
                int eventType = xpp.getEventType();
                while (eventType != XmlPullParser.END_DOCUMENT) {
                    if(eventType == XmlPullParser.START_DOCUMENT) {
                        System.out.println("Start document");
                    } else if(eventType == XmlPullParser.START_TAG) {
                        System.out.println("Start tag "+xpp.getName());
                        prevision = prevision + "<" + xpp.getName() + ">\n";
                    } else if(eventType == XmlPullParser.END_TAG) {
                        System.out.println("End tag "+xpp.getName());
                        prevision = prevision + "</" + xpp.getName() + ">\n";
                    } else if(eventType == XmlPullParser.TEXT) {
                        System.out.println("Text "+xpp.getText());
                        prevision = prevision + xpp.getText() + "\n";
                    }
                    eventType = xpp.next();
                }

                MainActivity.main.post(new Runnable() {
                    @Override
                    public void run() {
                        Message msg = MainActivity.main.obtainMessage();
                        Bundle bundle = new Bundle();
                        bundle.putString("Mensaje", prevision);
                        msg.setData(bundle);
                        MainActivity.main.sendMessage(msg);
                    }
                });
            }catch(XmlPullParserException f){
                Log.e("ERROR", f.toString());
            }

        }catch(IOException e) {
            Log.e("ERROR", e.toString());
        }finally {
            urlConnection.disconnect();
        }

        return null;
    }

}
