package com.example.euskalmet;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView texto;
    Button boton;
    Boolean con;
    static Handler main;
    EuskalmetReader emr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto = findViewById(R.id.textView);
        boton = findViewById(R.id.button);

        boton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(!conexion())
                    texto.setText("No hay conexión a internet");
                else {
                    emr = new EuskalmetReader();
                    emr.execute();
                    boton.setEnabled(false);
                }
            }
        });

        main = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                Bundle bundle = msg.getData();
                String mensaje = bundle.getString("Mensaje");
                texto.append("\n" + mensaje);
            }
        };
    }

    public Boolean conexion() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.es");

            int val = p.waitFor();
            boolean reachable = (val == 0);
            return reachable;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
