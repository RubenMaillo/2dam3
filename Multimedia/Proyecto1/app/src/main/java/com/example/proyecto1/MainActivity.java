package com.example.proyecto1;

import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView etiqueta1 = (TextView) findViewById(R.id.idTextView1);

        etiqueta1.setText("Hola Mundo 2DAM3");
    }
}
