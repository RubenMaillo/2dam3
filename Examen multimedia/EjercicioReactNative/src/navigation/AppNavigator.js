import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import MainScreen from 'EjercicioReactNative/src/screens/main'
import SecondaryScreen from 'EjercicioReactNative/src/screens/secondary'

export default createAppContainer(
    createStackNavigator(
        {
            Main: {screen: MainScreen},
            Secondary: {screen: SecondaryScreen}
        },
        {
            initialRouteName: "Main",
            defaultNavigationOptions: {
                headerStyle: {
                    backgroundColor: "#26A69A"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    fontWeight: "bold"
                }
            }
        }
    )
)