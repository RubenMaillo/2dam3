//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';

// create a component
class MainScreen extends Component {

    static navigationOptions = {
        title: "Primera pantalla"
    }

    constructor(props) {
        super(props)
      
        this.state = {
           //definir las variables de estado
           texto: "Texto de prueba pasado por navegación"
        };
    }

    navegar = () => {
        this.props.navigation.navigate("Secondary", { texto: this.state.texto });
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>  Pantalla principal  </Text>
                <Text>**********************</Text>
                <Button title="Navegar" onPress={this.navegar}/>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E0F2F1',
    },
});

//make this component available to the app
export default MainScreen;
