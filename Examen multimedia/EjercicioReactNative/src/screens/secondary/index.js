//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
class SecondaryScreen extends Component {

    static navigationOptions = {
        title: "Segunda pantalla"
    }

    constructor(props) {
        super(props)
      
        this.state = {
           //definir las variables de estado
           texto: ""
        };
    }

    componentDidMount = async () => {
        this.setState({texto: this.props.navigation.state.params.texto});
    }

    render() {
        return (
            <View style={styles.container}>
                <Text>  Segunda pantalla  </Text>
                <Text>********************</Text>
                <Text>{this.state.texto}</Text>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#E0F2F1',
    },
});

//make this component available to the app
export default SecondaryScreen;
