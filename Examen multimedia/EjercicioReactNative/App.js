/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { StatusBar } from "react-native"
import AppNavigator from "./src/navigation/AppNavigator"


class App extends Component{
    render(){
        return(
            <React.Fragment>
                <StatusBar backgroundColor="#26A69A" barStyle="dark-content" />
                <AppNavigator />
            </React.Fragment>
        );
    }
}

export default App;

