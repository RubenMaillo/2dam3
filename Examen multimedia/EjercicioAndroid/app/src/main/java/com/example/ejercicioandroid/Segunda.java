package com.example.ejercicioandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Segunda extends AppCompatActivity {

    boolean acertado = false;
    int numIntentos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);

        TextView saludo = (TextView) findViewById(R.id.saludo);
        saludo.setText("Hola " + getIntent().getExtras().getString("usuario"));

        final TextView intentos = (TextView) findViewById(R.id.intentos);

        intentos.setText("Numero de intentos: " + numIntentos);

        Button boton = (Button) findViewById(R.id.boton);

        boton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String numero = "2";
                EditText texto = (EditText) findViewById(R.id.texto);

                if(!acertado) {
                    if(numIntentos<10) {
                        if(!(texto.getText().toString().equals(""))) {
                            if (texto.getText().toString().equals(numero)) {
                                Toast.makeText(Segunda.this, "Has acertado el número", Toast.LENGTH_LONG).show();
                                acertado = true;
                            } else {
                                texto.setText("");
                                numIntentos = numIntentos + 1;
                                intentos.setText("Numero de intentos: " + numIntentos);
                            }
                        }
                    }
                    else{
                        Toast.makeText(Segunda.this, "Se ha terminado el juego", Toast.LENGTH_LONG).show();
                    }
                }
                else{
                    Toast.makeText(Segunda.this, "Se ha teminado el juego", Toast.LENGTH_LONG).show();
                }
            }
        });



    }
}
