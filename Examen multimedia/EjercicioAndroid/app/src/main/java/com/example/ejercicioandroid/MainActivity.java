package com.example.ejercicioandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String userName = "admin";
                String contrasena = "admin";

                EditText user = (EditText) findViewById(R.id.username);
                EditText pass = (EditText) findViewById(R.id.password);

                if(!((user.getText()).toString()).equals("") && !((pass.getText()).toString()).equals("")) {
                    if(((user.getText()).toString()).equals(userName) && ((pass.getText()).toString()).equals(contrasena)) {
                        Intent navegar = new Intent(MainActivity.this, Segunda.class);
                        navegar.putExtra("usuario", userName);
                        startActivity(navegar);
                    }
                    else{
                        user.setText("");
                        pass.setText("");
                        Toast.makeText(MainActivity.this, "Credenciales erroneas", Toast.LENGTH_LONG).show();
                    }
                }else
                    Toast.makeText(MainActivity.this, "Usuario o contraseña vacios", Toast.LENGTH_LONG).show();
            }
        });





    }
}
