import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

import MainScreen from 'Rescue/src/screens/main'
import SignIn from 'Rescue/src/screens/signin'
import SignUp from 'Rescue/src/screens/signup'
import Home from 'Rescue/src/screens/home'

export default createAppContainer(
    createStackNavigator(
        {
            Main: {screen: MainScreen},
            SignIn: {screen: SignIn},
            SignUp: {screen: SignUp},
            Home: {screen: Home},
        },
        {
            initialRouteName: "Main",
            /*defaultNavigationOptions: {
                headerStyle: {
                    backgroundColor: "#26A69A"
                },
                headerTintColor: "#fff",
                headerTitleStyle: {
                    fontWeight: "bold"
                }
            }*/
        }
    )
)