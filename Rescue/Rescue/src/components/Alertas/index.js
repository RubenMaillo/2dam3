import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Alert from 'Rescue/src/components/Alert';
import AllAlerts from 'Rescue/src/components/AllAlerts';
import AddAlert from 'Rescue/src/components/AddAlert';

class Alertas extends Component {
    constructor(props) {
        super(props);

        this.state = {
            addAlertsVisible: true,
            addModalVisible: false
        };
    }

    toggleModal = () => {
        this.setState({ addModalVisible: !this.state.addModalVisible });
    }

    toggleAlert = () => {
        this.setState({ addAlertsVisible: true });
    }

    toggleAllAlerts = () => {
        this.setState({ addAlertsVisible: false });
    }

  render() {
        const { addModalVisible, addAlertsVisible } = this.state;

        return (
            <View style={styles.cont}>
                <View style={styles.selecAlert}>
                    <TouchableOpacity style={ addAlertsVisible ? styles.misAlertas : styles.alertas } onPress={this.toggleAlert}>
                        <Text style={styles.texto}>Mis alertas</Text>
                    </TouchableOpacity>
                    <View style={styles.sepearador}></View>
                    <TouchableOpacity style={ !addAlertsVisible ? styles.misAlertas : styles.alertas } onPress={this.toggleAllAlerts}>
                        <Text style={styles.texto}>Todas</Text>
                    </TouchableOpacity>
                </View>

                {/* el conteido cambia dependiendo de la opcion selecionada de las alertas */}

                { addAlertsVisible ? <Alert func={this.toggleModal} /> : <AllAlerts/> }

                {/* end */}

                <AddAlert
                    style={styles.modal}
                    visible = {addModalVisible}
                    onCloseModal = {this.toggleModal}
                    //onAddAlert = {this.handleAdd}
                />
            </View>
            
        )
    }
}

// define your styles
const styles = StyleSheet.create({
    cont: {
        flex: 1,
        width: "100%",
        justifyContent: 'flex-start',
        alignItems: 'center',
    },
    selecAlert:{
        flexDirection: "row",
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000',
    },
    misAlertas: {
        width: "49.875%",
        height: 50,
        backgroundColor: "rgba(255, 255, 255, .15)",
        alignItems: "center",
        justifyContent: "center"
    },
    sepearador:{
        width: "0.25%",
        height: 30,
        backgroundColor: "#1EB27D",
    },
    alertas: {
        width: "49.875%",
        height: 50,
        backgroundColor: "#000000",
        alignItems: "center",
        justifyContent: "center"
    },
    texto: {
        color: "#e4e4e4",
        letterSpacing: 2,
    },
});

export default Alertas;
