//import liraries
import React, { Component } from 'react';
import { View, 
    Text, 
    StyleSheet, 
    Modal, 
    TextInput, 
    Image, 
    TouchableOpacity, 
    FlatList 
} from 'react-native';
import Socket  from 'Rescue/src/conexion/SocketIo';
import send from 'Rescue/src/assets/paper-plane-regular.png'
import close from 'Rescue/src/assets/times-solid.png'
import { ScrollView } from 'react-native-gesture-handler';
import { observer } from 'mobx-react'

function Item({ text }) {
    return (
        <TouchableOpacity style={styles.item}>
            <Text style={styles.title}>{text}</Text>
        </TouchableOpacity>
    );
}

@observer

// create a component
class Chat extends Component {

    constructor(props){
        super(props);

        this.state = {
            mensaje: "",
        }
    }


    renderItem(item, idex){
      return(
        <View style={styles.item}>
            <Text style={styles.title}>{item}</Text>
        </View>
      )
    }

    sendMessage = () => {
        Socket.mandar(this.state.mensaje);
        this.setState({ mensaje: "" })

        /*socket.emit("mensajeChat", this.state.mensaje);
        this.setState({ mensaje: "" })
        /*socket.on("mensajeChat", function(data){
            console.log(data);
        });
        this.setState({mensaje : ""});*/
    }

    render() {
      const { visible, onCloseModal } = this.props;

      return (
        <Modal visible={visible} transparent={true} animationType="slide">
        <View style={styles.container}>
            <View style={styles.content}>
                <View style={styles.header}>
                    <Text style = { styles.text }>CHAT</Text>
                    <TouchableOpacity style = {styles.close} onPress = {onCloseModal}>
                        <Image style = {styles.icon} source={close}/>
                    </TouchableOpacity>
                </View>

                <ScrollView style={styles.messages} contentContainerStyle={{flexGrow : 1, justifyContent : 'flex-end'}}>

                    {Socket.chat.map((item, index) => this.renderItem(item, index))}

                </ScrollView>
                
                <View style={styles.buttonRow}>
                    <TextInput 
                        style = {styles.input} 
                        placeholder="Mensaje" 
                        onChangeText = {texto => this.setState({mensaje: texto})}
                        multiline={true}
                        clearButtonMode="always"
                        value = {this.state.mensaje}
                    />
                    <TouchableOpacity style = {styles.send} onPress = {this.sendMessage}>
                        <Image style = {styles.icon} source={send}/>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    </Modal>
      );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "flex-end",
        flexDirection: "row",
        height: "100%",
      },
      content: {
        paddingBottom: 10,
        flex: 1,
        backgroundColor: "#ffffff",
        shadowOffset: { width: 0, height: -3 },
        shadowColor: "black",
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 30,
      },
      header: {
        width: "100%",
        height: 50,
        backgroundColor: "#1EB27D",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
      },
      closeIcon: {
        color: "#fff"
      },
      buttonRow: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingTop: 10,
        /*borderTopColor: "rgba(30, 178, 125, .5)",
        borderTopWidth: 2*/
      },
      block: {
        margin: 10
      },
      send: {
        width: 45,
        height: 45,
        borderRadius: 50,
        backgroundColor: "#1EB27D",
        justifyContent: "center",
        alignItems: "center",
      },
      close: {
        position: "absolute",
        right: 20
      },
      icon: {
        width: 20,
        height: 20,
      },
      messages: {
        width: "100%",
        height: 525,
      },
      input: {
        width: "80%",
        backgroundColor: "rgba(0,0,0,.1)",
        padding: 10,
        letterSpacing: 2,
        borderRadius: 50,
      },
      text: {
          color: "#e5e5e5",
          fontSize: 20,
          fontWeight: "bold"
      },
      item: {
        width: 150,
        backgroundColor: '#e7e7e7',
        margin: 10,
        padding: 20,
        marginVertical: 8,
        borderBottomRightRadius: 15,
        borderTopLeftRadius: 15,
        borderTopRightRadius: 15
    },
    title: {
        fontSize: 20,
        color: "#1EB27D",
    },
});

//make this component available to the app
export default Chat;
