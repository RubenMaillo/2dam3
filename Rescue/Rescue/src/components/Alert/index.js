//import liraries
import React, {Component} from 'react';
import { View, Text, StyleSheet } from 'react-native';
import FAB from 'Rescue/src/components/FAB';
import Home from 'Rescue/src/screens/home';


class MyAlerts extends Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (
            <React.Fragment>
                <FAB 
                    text="+"
                    fabStyle={{backgroundColor: "#1EB27D"}}
                    textStyle={{color: "#e4e4e4"}}
                    onPress={this.props.func}
                />
            </React.Fragment>
        )
    }
}

//make this component available to the app
export default MyAlerts;
