import React, { Component } from 'react';
import { 
  View, 
  Text, 
  StyleSheet, 
  TextInput,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {aUsuario} from 'Rescue/src/screens/signin'

class Profile extends Component {

  constructor(props) {
    super(props);

    this.state = {
      user: {},
    };
  }

  componentDidMount = async () =>{
    
    console.log(aUsuario);
  }

  render() {
    return (
      <React.Fragment>
        <View style={styles.container}>
            <Text style={styles.text}>Name</Text>
            <TextInput style={styles.input} placeholder="Name" value={aUsuario[1]} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Last name</Text>
            <TextInput style={styles.input} placeholder="Last name" value={aUsuario[2]} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>DNI</Text>
            <TextInput style={styles.input} placeholder="DNI" value={aUsuario[3]} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Age</Text>
            <TextInput style={styles.input} placeholder="Age" value={aUsuario[4].toString()} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Gender</Text>
            <TextInput style={styles.input} placeholder="Gender" value={aUsuario[5]} placeholderTextColor="#E5E5E5" />

            <Text style={styles.text}>Phone number</Text>
            <TextInput style={styles.input} placeholder="Phone number" value={aUsuario[6]} placeholderTextColor="#E5E5E5" />
        </View>
      </React.Fragment>
    );
  }
}

// define your styles
const styles = StyleSheet.create({
  container: {
      flex: 1,
      width: "100%",
      justifyContent: 'center',
      alignItems: 'center',
  },
  cont: {
      width: 40,
      height: 40,
      marginRight: 20,
      justifyContent: "center",
      alignItems: "center",
  },
  icon: {
      width: 22,
      height: 25,
  },
  text: {
    fontSize: 15,
    color: "#1EB27D",
  },
  input: {
    width: "70%",
    height: 40,
    textAlign: "center",
    backgroundColor: "rgba(0, 0, 0, .8)",
    margin: 10,
    letterSpacing: 3,
    color: "#E5E5E5",
    fontSize: 15,
  },
});

export default Profile;
