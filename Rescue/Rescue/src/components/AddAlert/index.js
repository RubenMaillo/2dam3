//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Modal, Button } from 'react-native';
import BasicAddItems from 'Rescue/src/components/BasicAddItems';

// create a component
class MyClass extends Component {
    render() {
      const { visible, onCloseModal } = this.props;

      return (
          <Modal visible={visible} transparent={true} animationType="slide">
              <View style={styles.container}>
                  <View style={styles.content}>
                      <BasicAddItems></BasicAddItems>
                      <View style={styles.buttonRow}>
                          <Button title="Cancelar" onPress={onCloseModal} color="#ff0000" />
                          <Button title="Enviar aviso" onPress={this.addTodo} />
                      </View>
                  </View>
              </View>
          </Modal>
      );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "flex-end",
        flexDirection: "row",
      },
      content: {
        padding: 20,
        paddingBottom: 30,
        flex: 1,
        backgroundColor: "#ffffff",
        shadowOffset: { width: 0, height: -3 },
        shadowColor: "black",
        shadowOpacity: 0.2,
        shadowRadius: 5,
        elevation: 30
        //width: 200
      },
      text: {
        borderBottomWidth: 1,
        padding: 5
      },
      closeIcon: {
        color: "#fff"
      },
      buttonRow: {
        flexDirection: "row",
        justifyContent: "space-around",
        paddingBottom: 20
      },
      block: {
        margin: 10
      }
});

//make this component available to the app
export default MyClass;
