//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import { getAllAlerts } from 'Rescue/src/conexion'

function Item({ title, descripcion }) {
    return (
        <TouchableOpacity style={styles.item}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.descripcion}>{descripcion}</Text>
        </TouchableOpacity>
    );
}

class AllAlerts extends Component {

    constructor(props) {
        super(props);

        this.state = {
            numAlertas: 0,
            alertas: [],
        }
    }

    componentDidMount() {
        getAllAlerts().then((alerts) => {
            this.setState({numAlertas: alerts["json"].length, alertas: alerts["json"]});
            //console.log("alertas"+this.state.alertas[0]['nombre']);
        });
    }

    render() {

        const { numAlertas, alertas } = this.state;
        //console.log(alertas);

        return (
            <React.Fragment>
                <SafeAreaView style={styles.container}>
                    <ScrollView  style={styles.scroll}>
                        <FlatList
                            data={alertas}
                            renderItem={({ item, index }) => <Item title={this.state.alertas[index]["nombre"]} descripcion={this.state.alertas[index]["descripcion"]} />}
                            //keyExtractor ={({ item, index }) => this.state.alertas[index]["_id"]}
                        />           
                    </ScrollView>
                </SafeAreaView>
            </React.Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        justifyContent: "center",
        alignItems: "center",
    },
    scroll: {
        width: "95%",
    },
    item: {
        width: "100%",
        backgroundColor: '#f4f4f4',
        //backgroundColor: '#f7f7f7',
        padding: 20,
        marginVertical: 8,
        borderRadius: 10,
    },
    title: {
        fontSize: 20,
        color: "#1EB27D",
    },
    descripcion: {
        fontSize: 15,
    },
  });

//make this component available to the app
export default AllAlerts;
