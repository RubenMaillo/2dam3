import React from 'react';
import { Text, TouchableOpacity, StyleSheet } from 'react-native';

const size = 50;

const styles = StyleSheet.create({
    container: {
      height: size,
      width: size,
      borderRadius: size / 2,
      justifyContent: "center",
      alignItems: "center",
      elevation:5,
      position: "absolute",
      bottom: 15,
      right: 15
    },
    plus: {
      fontSize: 25
    }
});

const FAB = (props) => {
    const { fabStyle, textStyle, text, ...otherProps } = props
    return(
        <TouchableOpacity style={[styles.container, fabStyle]} {...otherProps}>
            <Text style={[styles.plus, textStyle]}>{text}</Text>
        </TouchableOpacity>
    )
};

export default FAB;
