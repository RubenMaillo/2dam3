import React from "react";
import {
  StyleSheet,
  Modal,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Button
} from "react-native";

const styles = StyleSheet.create({
  descripcion: {
    borderBottomWidth: 1,
    borderColor: "#1EB27D",
  },
  block: {
    margin: 10,
    marginBottom: 15,
  }
});


const BasicAddItems = () => (
  <React.Fragment>
    <View style={styles.block}>
      <Text>Descripción:</Text>
      <TextInput
        placeholder = "Introduce una descripción de la alerta"
        style={styles.descripcion}
        numberOfLines={2}
        multiline={true}
        clearButtonMode="always"
      />
    </View>
  </React.Fragment>
);

export default BasicAddItems;