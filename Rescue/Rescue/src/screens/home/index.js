//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, Image, ToastAndroid } from 'react-native';

import Alertas from 'Rescue/src/components/Alertas';
import Mapa from 'Rescue/src/components/map';
import Perfil from 'Rescue/src/components/profile';

import alerts from 'Rescue/src/assets/bell-solid.png';
import alertsGreen from 'Rescue/src/assets/bell-green.png';
import map from 'Rescue/src/assets/map-marker-alt-solid.png';
import mapGreen from 'Rescue/src/assets/map-green.png';
import user from 'Rescue/src/assets/user-regular.png';
import userGreen from 'Rescue/src/assets/user-green.png';

// create a component
class Home extends Component {

    static navigationOptions = {
        title: "RESCUE",
        headerTintColor: "#e4e4e4",
        headerStyle: {
            backgroundColor: 'black'
        },
    }

    constructor(props) {
        super(props)
      
        this.state = {
            addAlertsVisible: true,
            addMapVisible: false,
            addProfileVisible: false,
        };
    }

    toggleViews = view =>{
        if (view === "Alerts")
            this.setState({ addAlertsVisible: true, addMapVisible: false, addProfileVisible: false });

        if (view === "Map")
            this.setState({ addAlertsVisible: false, addMapVisible: true, addProfileVisible: false });
    
        if (view === "Profile")
            this.setState({ addAlertsVisible: false, addMapVisible: false, addProfileVisible: true });  
    }

    render() {

        const { addAlertsVisible, addMapVisible, addProfileVisible } = this.state;

        return (
            <View style={styles.container}>
                <View style={styles.mainCont}>
                    { addAlertsVisible ? <Alertas/> : addMapVisible ? <Mapa/> : <Perfil/> }
                </View>
                <View style={styles.menubar}>
                    <TouchableOpacity 
                        style={addAlertsVisible ? styles.contThis : styles.cont} 
                        onPress={ () => this.toggleViews("Alerts") }
                    >
                        <Image style={styles.icon} source={addAlertsVisible ? alertsGreen : alerts}/>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={addMapVisible ? styles.contThis : styles.cont}
                        onPress={ () => this.toggleViews("Map") }
                    >
                        <Image style={styles.icon} source={addMapVisible ? mapGreen : map}/>
                    </TouchableOpacity>

                    <TouchableOpacity 
                        style={addProfileVisible ? styles.contThis : styles.cont}
                        onPress={ () => this.toggleViews("Profile") }
                    >
                        <Image style={styles.icon} source={addProfileVisible ? userGreen : user}/>
                    </TouchableOpacity>
                </View>

                
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        //justifyContent: 'center',
        alignItems: 'center',
        justifyContent: "center",
        //backgroundColor: '#ffffff',
    },
    mainCont: {
        flex: 0.90,
        width: "100%",
        justifyContent: 'flex-start',
        alignItems: 'center',
        
    },
    selecAlert:{
        flexDirection: "row",
        width: "100%",
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#000000',
    },
    misAlertas: {
        width: "49.875%",
        height: 50,
        backgroundColor: "rgba(255, 255, 255, .15)",
        alignItems: "center",
        justifyContent: "center"
    },
    sepearador:{
        width: "0.25%",
        height: 30,
        backgroundColor: "#1EB27D",
    },
    alertas: {
        width: "49.875%",
        height: 50,
        backgroundColor: "#000000",
        alignItems: "center",
        justifyContent: "center"
    },
    menubar: {
        flex: 0.10,
        flexDirection: "row",
        width: "100%",
        backgroundColor: '#000000',
        bottom: 0,
        justifyContent: 'space-around',
        alignItems: 'center',
    },
    texto: {
        color: "#e4e4e4",
        letterSpacing: 2,
    },
    fab: {
        height: 50,
        width: 50,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        elevation:5,
        position: "absolute",
        bottom: 20,
        right: 20,
        backgroundColor: "#1EB27D"
    },
    contThis: {
        width: 40,
        height: 40,
        backgroundColor: "rgba(119, 229, 229, .3)",
        borderWidth: 1,
        borderRadius: 50,
        borderColor: "#1EB27D",
        marginRight: 20,
        justifyContent: "center",
        alignItems: "center"
    },
    cont: {
        width: 40,
        height: 40,
        marginRight: 20,
        justifyContent: "center",
        alignItems: "center",
    },
    icon: {
        width: 22,
        height: 25,
    },
});

//make this component available to the app
export default Home;
