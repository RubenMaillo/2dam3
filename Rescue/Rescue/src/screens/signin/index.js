//import liraries
import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    ImageBackground, 
    TouchableOpacity, 
    TextInput, 
    ToastAndroid,
    DeviceEventEmitter,
} from 'react-native';
import { login, getCurrentUser } from 'Rescue/src/conexion'
import AsyncStorage from '@react-native-community/async-storage';
//import { login } from 'Rescue/src/conexion/SocketIo'


export var aUsuario;
// create a component
class SignIn extends Component {

    static navigationOptions = {
        headerTransparent: {
            backgroundColor: 'transparent',
        }
    }

    constructor(props) {
        super(props)
      
        this.state = {
           //definir las variables de estado
           user: "",
           pass: "",
           
        };

        this.userInput = React.createRef()
        this.passInput = React.createRef()
    }

    /*componentDidMount = async () => {
        this.setState({texto: this.props.navigation.state.params.texto});
    }*/

    signIn() {
        var nav = this.props;
        login(this.state.user, this.state.pass)
        .then(function(response){
            var json = response.json();

            json.then((log) => {
                if(log["ok"]){
                    
                    aUsuario = [log["json"]["_id"], 
                                log["json"]["nombre"], 
                                log["json"]["apellidos"],
                                log["json"]["dni"], 
                                log["json"]["edad"], 
                                log["json"]["sexo"],  
                                log["json"]["telefono"]];

                    nav.navigation.navigate("Home");
                }
                else
                    alert("Credenciales incorrectas");
            });
        });
    }

    signUp() {
        this.props.navigation.navigate("SignUp");
    }

    render() {
        return (
            <ImageBackground style={styles.fondo} resizeMode='cover' source={require('../../images/montaña.jpg')}>
                <View style={styles.capaFondo}>
                    <View style={styles.container}>
                        <TextInput style={styles.input} placeholder="User" placeholderTextColor="#E5E5E5" onChangeText = { (user) => this.setState({user: user})} />
                        <TextInput secureTextEntry={true} style={styles.input} placeholder="Pass" placeholderTextColor="#E5E5E5" onChangeText = { (pass) => this.setState({pass: pass})} />

                        <Text style={styles.text}>
                            ¿You still do not have an account?
                            <Text style={styles.link} onPress={() => this.signUp}>Click here.</Text>
                        </Text>

                        <TouchableOpacity style={styles.signin} onPress={() => this.signIn() }>
                            <Text style={styles.texto}>Sign in</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    fondo:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        //fontFamily: "Raleway-Regular"
    },
    capaFondo: {
        flex: 1,
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, .7)"
    },
    container: {
        width: "80%",
        height: "75%",
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: "rgba(0, 0, 0, .4)"
    },
    signin: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 150,
        height: 50,
        borderRadius:50,
        borderWidth: 2,
        borderColor: "#1EB27D",
        margin: 10,
    },
    input: {
        width: "75%",
        textAlign: "center",
        backgroundColor: "rgba(0, 0, 0, .5)",
        margin: 10,
        letterSpacing: 3,
        color: "#E5E5E5",
    },
    texto: {
        color: "#1EB27D"
    },
    text: {
        color: "#E5E5E5",
        letterSpacing: 2,
        fontSize: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    link: {
        color: "#0072F9"
    }
});

//make this component available to the app
export default SignIn;
