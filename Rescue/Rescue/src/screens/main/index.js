//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, ImageBackground, TouchableOpacity} from 'react-native';
import { conectar } from 'Rescue/src/conexion'
//import { connect } from 'Rescue/src/conexion/MWebSocket'
import { connect } from 'Rescue/src/conexion/SocketIo'

// create a component
class MainScreen extends Component {

    static navigationOptions = {
        header: null,
        headerMode: 'none'
    }

    constructor(props) {
        super(props)
      
        this.state = {
           //definir las variables de estado
           texto: "Texto de prueba pasado por navegación"
        };

        //connect();
        conectar();
    }

    signIn = () => {
        this.props.navigation.navigate("SignIn", { texto: this.state.texto });
    }

    signUp = () => {
        this.props.navigation.navigate("SignUp", { texto: this.state.texto });
    }

    render() {
        return (
            
            <ImageBackground style={styles.fondo} resizeMode='cover' source={require('../../images/montaña.jpg')}>
                <View style={styles.capaFondo}>

                    <Text style={styles.nombre}>RESCUE</Text>

                    {/* <TouchableOpacity style={styles.signin} onPress = { () => mensaje() } >
                        <Text>Mensaje</Text>
                    </TouchableOpacity> */}

                    <View style={styles.container}>
                        <TouchableOpacity style={styles.signin} onPress={this.signIn}>
                            <Text style={styles.texto}>Sign in</Text>
                        </TouchableOpacity>

                        <TouchableOpacity style={styles.signup} onPress={this.signUp}>
                            <Text style={styles.signupName}>Sign up</Text>
                        </TouchableOpacity>
                    </View>

                </View>
            </ImageBackground>
            
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    fondo:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    capaFondo: {
        flex: 1,
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, .4)"
    },
    container: {
        position: "absolute",
        bottom: 0,
        flexDirection: "row",
        width: "120%",
        height: "22%",
        backgroundColor: "rgba(0, 0, 0, .5)",
        justifyContent: 'center',
        alignItems: 'center',
        borderTopEndRadius: 200,
        borderTopStartRadius: 200,
    },
    signin: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        height: 50,
        borderRadius:50,
        borderWidth: 2,
        borderColor: "#1EB27D",
        margin: 20,
    },
    signup: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 100,
        height: 50,
        borderRadius:50,
        backgroundColor: "#1EB27D",
        margin: 20,
    },
    signupName: {
        color: "rgba(0, 0, 0, .7)",
    },
    texto: {
        color: "#1EB27D"
    },
    nombre: {
        position: "absolute",
        color: "#b2b2b2",
        fontSize: 50,
        letterSpacing: 15,
    }
});

//make this component available to the app
export default MainScreen;
