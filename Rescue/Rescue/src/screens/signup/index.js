//import liraries
import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    ImageBackground, 
    TouchableOpacity, 
    TextInput, 
    ToastAndroid,
    Picker,
} from 'react-native';

const genders = ["Male", "Female", "Other"];

// create a component
class SignUp extends Component {

    static navigationOptions = {
        headerTransparent: {
            backgroundColor: 'transparent',
        }
    }

    constructor(props) {
        super(props)
      
        this.state = {
            //definir las variables de estado
            name: "",
            lastName: "",
            age: 0,
            gender: "",
            phoneNumber: "",
            user: "",
            pass: "",
        };
    }

    /*componentDidMount = async () => {
        this.setState({texto: this.props.navigation.state.params.texto});
    }*/

    signUp = () => {
        ToastAndroid.show('Vas a registrarte', ToastAndroid.SHORT);
    }

    render() {
        return (
            <ImageBackground style={styles.fondo} resizeMode='cover' source={require('../../images/montaña.jpg')}>
                <View style={styles.capaFondo}>
                    <View style={styles.container}>
                        <TextInput style={styles.input} placeholder="Name" placeholderTextColor="#E5E5E5" />
                        <TextInput style={styles.input} placeholder="Last name" placeholderTextColor="#E5E5E5" />
                        <TextInput style={styles.input} placeholder="Age" placeholderTextColor="#E5E5E5" />
                        
                        <Picker
                            placeholder="Gender"
                            onValueChange={itemValue => onChange(itemValue)}
                            style={styles.input}
                            itemStyle={{ backgroundColor: "grey", color: "blue", fontFamily:"Ebrima", fontSize:17 }}
                        >
                            {genders.map((item, idx) => (
                                <Picker.Item style={styles.item} label={item} value={idx} />
                            ))}
                        </Picker>

                        <TextInput style={styles.input} placeholder="Phone number" placeholderTextColor="#E5E5E5" />
                        <TextInput style={styles.input} placeholder="User" placeholderTextColor="#E5E5E5" />
                        <TextInput secureTextEntry={true} style={styles.input} placeholder="Pass" placeholderTextColor="#E5E5E5" />

                        <TouchableOpacity style={styles.signup} onPress={this.signIn}>
                            <Text style={styles.texto}>Sign up</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ImageBackground>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    fondo:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    capaFondo: {
        flex: 1,
        width: "100%",
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: "rgba(0, 0, 0, .7)"
    },
    container: {
        width: "80%",
        height: "75%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    signup: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 150,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#1EB27D",
        margin: 20,
    },
    input: {
        width: "75%",
        textAlign: "center",
        backgroundColor: "rgba(0, 0, 0, .5)",
        margin: 10,
        letterSpacing: 3,
        color: "#E5E5E5",
    },
    texto: {
        color: "rgba(0, 0, 0, .7)",
    },
    text: {
        color: "#E5E5E5",
        letterSpacing: 2,
        fontSize: 10,
        marginTop: 10,
        marginBottom: 10,
    },
    link: {
        color: "#0072F9"
    },
    item: {
        color: "rgba(0, 0, 0, .7)",
    }
});

//make this component available to the app
export default SignUp;
