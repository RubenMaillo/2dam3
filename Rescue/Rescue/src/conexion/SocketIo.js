import {ToastAndroid} from 'react-native'
import socketIO from 'socket.io-client';
import { observable, action, computed } from 'mobx';

//const URLServer = "http://192.168.0.14:2222";
const URLServer = "http://10.18.49.115:2222";



// create a component
class Socket {

   socket = socketIO.connect(URLServer, { 'forceNew': true });

   @observable _chat = ["Hola", "Adios", "Hola", "Adios","Hola", "Adios","Hola", "Adios","Hola", "Adios","Hola", "Adios",]

   @action setChat(valor){
      this._chat = valor
   }

   @computed get chat(){
      return this._chat
   }
   
   constructor(){
      this.inicializarHandlers(this);
   }

   verChat(){
      
   }

   inicializarHandlers(t){
      //socket
      this.socket.on("connect", function(){
         ToastAndroid.show("Conectado al servidor en el puerto 2222", ToastAndroid.SHORT);
      });

      this.socket.on("mensajeChat", function(data){
         t.chat.push(data);
      });
   }

   mandar(mensaje){
      this.socket.emit("mensajeChat", mensaje);
   }
}

let socket = new Socket()
export default socket;








