//import liraries
import { DeviceEventEmitter, ToastAndroid } from 'react-native'
import Socket from 'react-native-sockets'

export function inicializarListeners(){
    //on connected
    DeviceEventEmitter.addListener('socketClient_connected', () => {
        //console.log('socketClient_connected');
        ToastAndroid.show('Conectado', ToastAndroid.SHORT);
    });

    DeviceEventEmitter.addListener('socketClient_error', (data) => {
        console.log('socketClient_error',data.error);
        ToastAndroid.show('Error', ToastAndroid.SHORT);
    });

    //on client closed
    DeviceEventEmitter.addListener('socketClient_closed', (data) => {
        console.log('socketClient_closed',data.error);
        ToastAndroid.show('Desconectado', ToastAndroid.SHORT);
    });
}

export function conectar(){
    var config = {
        address: "10.18.49.115", //ip address of server (clase)
        //address : "192.168.0.14", //ip address of server (casa)
        port: 3000, //port of socket server
    }

    inicializarListeners();

    Socket.startClient(config);
}

export function mensaje(){
    Socket.write("Soy un cliente\n");
}

export function login(user, pass){
    //http://10.18.49.115:3000/login
    //http://192.168.0.14:3000/login

    
    var login = fetch("http://10.18.49.115:3000/login", {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            user: user,
            pass: pass,
        }),
    });

    return login;
}

export function getAllAlerts(){
    return fetch('http://10.18.49.115:3000/alertas')
    .then((response) => response.json())
    .then((responseJson) => {
        return responseJson;
    })
    .catch((error) => {
        console.error(error);
    });
}


