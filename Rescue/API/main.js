var express = require('express'),
    bodyParser = require('body-parser'),
    mongoose = require('mongoose');

api = express();

var server = require('http').Server(api);
var io = require('socket.io')(server);

api.use(bodyParser.urlencoded({ extended: false }));
api.use(bodyParser.json());
//api.use(methodOverride());

// CORS
var cors = require('cors');
api.use(cors());
api.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

mongoose.connection.openUri('mongodb://localhost:27017/Rescue', (err, res) => {
    if(err) {
        console.log('ERROR: connecting to Database. ' + err);
    }
});


const rutaUsuario = require('./routes/RUsuario');
const rutaLogin = require('./routes/RLogin');
const rutaAlertas = require('./routes/RAlertas');

api.use("/usuario", rutaUsuario);
api.use("/login", rutaLogin);
api.use("/alertas", rutaAlertas);


//Socket API
var port = 3000;
api.listen(port, function() {
  console.log("Express corriendo en el puerto " + port);
});

//Socket.io SERVER
server.listen(2222, function(socket){
  console.log('Servidor corriendo en el puerto 2222');
});

io.on('connection', function(socket) {
  console.log('Un cliente se ha conectado');
  
  socket.on("mensajeChat", function(data){
    console.log("Cliente: " + data);
    socket.emit("mensajeChat", data);
  });
});

