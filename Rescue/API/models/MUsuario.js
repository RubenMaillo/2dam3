var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var UsuarioSchema = new Schema({
    dni: { type: String, required: [true, 'El DNI es obligatorio'] },
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    apellidos: { type: String, required: [true, 'Los apellidos son obligatorios'] },
    telefono: { type: String, required: [true, 'El teléfono es obligatorio'] },
    password: { type: String, required: [true, 'La contrasña es obligatoria'] },
    admin: { type: Boolean },
});

module.exports = mongoose.model('usuario', UsuarioSchema);