var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var AlertaSchema = new Schema({
    nombre: { type: String, required: [true, 'El nombre es obligatorio'] },
    descripcion: { type: String, required: [true, 'La descripción es obligatoria'] },
    zona: { type: String, required: [true, 'El nombre de la zona es obligatorio'] },
    fechaCreacion: { type: Date, required: [true, 'La fecha es obligatoria'] },
    coordenadas: { type: [String], required: [true, 'Las coordenadas son obligatorias'] },
    esActiva: { type: Boolean },

});

module.exports = mongoose.model('alerta', AlertaSchema);