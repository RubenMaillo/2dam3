var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var ZonaBusquedaSchema = new Schema({
    id_alerta: { type: Schema.Types.ObjectId, ref: 'MAlerta'},
    id_usuario: { type:[Schema.Types.ObjectId], ref: 'MUsuario' },
    fechaCreacion: { type: Date, required: [true, 'La fecha es obligatoria'] },
    zona: { type: String, required: [true, 'El nombre de la zona es obligatorio'] },
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
});

module.exports = mongoose.model('alerta', ZonaBusquedaSchema);