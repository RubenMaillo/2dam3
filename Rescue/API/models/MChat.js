var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var TrakSchema = new Schema({
    id_alerta: { type: Schema.Types.ObjectId, ref: 'MAlerta'},
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
});

module.exports = mongoose.model('alerta', TrakSchema);