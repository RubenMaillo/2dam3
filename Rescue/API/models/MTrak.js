var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var TrakSchema = new Schema({
    id_zonaBusqueda: { type: Schema.Types.ObjectId, ref: 'MZonaBusqueda'},
    id_usuario: { type:Schema.Types.ObjectId, ref: 'MUsuario' },
    coordenadas: { type: [JSON], required: [true, 'Las coordenadas son obligatorias'] },
});

module.exports = mongoose.model('alerta', TrakSchema);