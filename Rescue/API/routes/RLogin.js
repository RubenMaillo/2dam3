var express = require('express');
var app = express();
var loginController = require('../controllers/CLogin.js')

app.post('/', loginController.logUsuario);

module.exports = app;
