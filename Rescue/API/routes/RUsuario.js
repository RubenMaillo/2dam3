var express = require('express');
var app = express();
var CUsuario = require('../controllers/CUsuario.js');

app.get('/', CUsuario.getAllUsers); // Visualiza TODOS los usuarios
app.post('/getCurrentUser', CUsuario.getUser); // Visualiza TODOS los usuarios
app.post('/', CUsuario.insertUser); // Añade un usuario

module.exports = app;
