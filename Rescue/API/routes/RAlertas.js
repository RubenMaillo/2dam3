var express = require('express');
var app = express();
var CAlerta = require('../controllers/CAlerta.js')

app.get('/', CAlerta.getAllAlertas); // Recoger todas las alertas
app.post('/', CAlerta.addAlerta); // Añadir una alerta
app.post('/modAlerta', CAlerta.modAlerta); // Modificar una alerta
app.post('/delAlerta', CAlerta.delAlerta); // Eliminar una alerta

module.exports = app;
