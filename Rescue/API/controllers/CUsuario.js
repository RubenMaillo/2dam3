var Usuario = require('../models/MUsuario');
var bcrypt = require('bcryptjs');

function getAllUsers(req, res, next) {
    Usuario.find({}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function getUser(req, res, next){

    const { user } = req.body;

    Usuario.findOne({dni: user}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
};


function insertUser(req, res, next){
    var { dni, nombre, apellidos, telefono, password, /*admin*/ } = req.body;

    var usuario = new Usuario({
        dni: dni,
        nombre: nombre,
        apellidos: apellidos,
        telefono: telefono,
        password: bcrypt.hashSync(password, 10),
        //admin: admin
    });

    usuario.save(( err, usuarioGuardado ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear el usuario',
                errors: err
            });
        }
        /*res.status(201).json({
            ok: true,
            usuario: usuarioGuardado
        });*/
    });
}

module.exports = {
    getAllUsers,
    getUser,
    insertUser
};

