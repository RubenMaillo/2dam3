var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');
var SEED = require('../config/config').SEED;
var Usuario = require('../models/MUsuario');

function logUsuario(req, res, next){

    const { user, pass } = req.body;

    console.log("Usuario" + user + pass);

    Usuario.findOne( {dni: user}, (err, usuarioDB)=>{
        
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuarios',
                errors: err
            });
        }
        
        if (!usuarioDB){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
            
        }

        if( !bcrypt.compareSync(pass, usuarioDB.password)){
            return res.status(400).json({
                ok: false,
                mensaje: 'Credenciales incorrectas',
                errors: err
            });
        }

        usuarioDB.password = 'Cambiamos la contraseña';
        var token = jwt.sign({ usuario: usuarioDB }, SEED, {expiresIn: 14400 }) //4 el tokan expira en cuatro horas

        res.status(200).json({
            ok: true,
            json: usuarioDB,
            token: token,
            id: usuarioDB._id,
        });
    });
};

module.exports = {
    logUsuario
};
