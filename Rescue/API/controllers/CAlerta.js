var bcrypt = require('bcryptjs');
var Alerta = require('../models/MAlerta');

function getAllAlertas(req, res, next) {
    Alerta.find({}).exec((err, json) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error de base de datos',
                errors: err
            });
        }
        
        res.status(200).json({
            ok: true,
            json
        });
    });
}

function addAlerta(req, res, next){
    var { nombre, descripcion, zona, fechaCreacion, coordenadas, esActiva } = req.body;

    console.log(nombre + " / " + descripcion + " / " + zona + " / " + fechaCreacion + " / " + coordenadas + " / " + esActiva);

    alerta = new Alerta({
        nombre: nombre,
        descripcion: descripcion,
        zona: zona,
        fechaCreacion: fechaCreacion,
        coordenadas: coordenadas,
        esActiva: esActiva,
    });

    alerta.save( ( err, alertaGuardada ) => {
        if (err){
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear la alerta',
                errors: err
            });
        }
        res.status(201).json({
            ok: true,
            alerta: alertaGuardada
        });
    });
}

function modAlerta(req, res, next){
    var { id, nombre, descripcion, zona, fechaCreacion, coordenadas, esActiva } = req.body;

    Alerta.findById(id, (err, alerta) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la búsqueda',
                errors: err
            });
        }

        if (!BusquedaBorrada){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe esa búsqueda con ese id',
            });
        }


        alerta.nombre = nombre;
        alerta.descripcion = descripcion;
        alerta.zona = zona;
        alerta.fechaCreacion = alerta.fechaCreacion;
        alerta.coordenadas = coordenadas;
        alerta.esActiva = esActiva;

        alerta.save((err, alertaGuardada) => {
            if (err){
                return res.status(400).json({
                    ok: false,
                    mensaje: 'Error al actualizar el usuario',
                    errors: err
                });
            }

            res.status(200).json({
                ok: true,
                paciente: alertaGuardada
            });
        })

    })
}

function delAlerta(req, res, next){
    var { id } = req.body;

    Alerta.findByIdAndRemove( id, ( err, alertaDel ) => {
        if (err){
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al borrar la alerta',
                errors: err
            });
        }

        if (!alertaDel){
            return res.status(400).json({
                ok: false,
                mensaje: 'No existe una alerta con ese ID',
            });
        }

        res.status(200).json({
            ok: true,
            consulta: alertaDel
        });
    })
    
}

module.exports = {
    getAllAlertas,
    addAlerta,
    modAlerta,
    delAlerta,
};

