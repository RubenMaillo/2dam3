import java.io.IOException;
import java.util.Arrays;

class RunProcess {
	public static void main(String[] args) throws Exception{
		if(args.length <= 0){
			System.err.println("Se necestia un programa a ejecutar");
			System.exit(-1);
		}

		ProcessBuilder pb = new ProcessBuilder(args);
		try {
			Process process = pb.start();
			int retorno = process.waitFor();
			System.out.println("La ejecucion de " + Arrays.toString(args) + " devuelve " + retorno);
		} catch (Exception e) {
			System.err.println("Excepcion de E/S!!");
			System.exit(-1);
		} 
		if (Thread.interrupted())  // Clears interrupted status!
			throw new InterruptedException();
	}
}
