package org.prueba.websocket.cliente;

import java.net.UnknownHostException;
import java.net.http.WebSocket;

import org.prueba.websocket.servidor.Servidor;

public class Cliente {

	public static void main(String[] args) {
		try {
			Servidor client = new Servidor(3333);
			client.start();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
	}
	
}
