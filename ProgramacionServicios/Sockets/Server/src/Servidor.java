import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.ServerSocket;
import java.net.Socket;

public class Servidor extends Thread{
	
	Socket con;
	
	public Servidor(Socket con) {
		this.con = con;
	}

	public static void main(String[] args) {
		try {
			iniciar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public static void iniciar() throws IOException {
		ServerSocket server = new ServerSocket(3333);
		while(true) {
			try {
			
				Socket client = server.accept();
				System.out.println("alguien se ha conectado");
				
				new Servidor(client).start();
			
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
		}
		
		server.close();
		
	}
	
	public void run() {
		
		try {
			
			System.out.println("Cliente conectado --> " + con.getRemoteSocketAddress().toString());
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
			bw.write("Conectado\n");
			bw.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String linea = null;
			linea = br.readLine();
			
		
			while(!linea.equalsIgnoreCase("END")) {
				System.out.println("Cliente " + con.getRemoteSocketAddress().toString() + ": "+linea);
				linea = br.readLine();
			}
			
			
			System.out.println("Conexi�n cerrada con el cliente " + con.getRemoteSocketAddress().toString() + "\n");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


}