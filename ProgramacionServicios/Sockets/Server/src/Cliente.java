import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente{
	
	public static void main(String[] args) {
		iniciar();
	}
	
	public static void iniciar() {
		try {
			
			Scanner sc = new Scanner(System.in);
			
			Socket clientSocket = new Socket();
			InetSocketAddress isa = new InetSocketAddress("localhost", 3333);
			clientSocket.connect(isa);
			
			InputStream is = clientSocket.getInputStream();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			
			System.out.println(br.readLine());
			
			OutputStream os = clientSocket.getOutputStream();
			
			String mensaje = "";
			
			while(!mensaje.equalsIgnoreCase("END")) {
				System.out.print("Escribe un mensaje --> ");
				
				mensaje = sc.nextLine();
				
				String salto = mensaje + "\n";
			
				os.write(salto.getBytes());
				os.flush();
			}
			
			clientSocket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
