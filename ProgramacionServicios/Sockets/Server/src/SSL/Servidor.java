package SSL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;

public class Servidor{
	
	public static List<Socket> clientes = new ArrayList<Socket>();
	Socket con;
	
	public Servidor(Socket con) {
		this.con = con;
	}

	public static void main(String[] args) {
		try {
			//iniciar();
			iniciarSSL();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void iniciarSSL() throws IOException {
		
		SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		SSLServerSocket server = (SSLServerSocket) ssf.createServerSocket();
		
		InetSocketAddress addr = new InetSocketAddress("localhost", 3333);
		server.bind(addr);
		
		SSLSocket socket = (SSLSocket) server.accept();
		InputStream is = socket.getInputStream();
		
		byte[] mensaje = new byte[25];
		is.read(mensaje);
		
		socket.close();
		
	}
}