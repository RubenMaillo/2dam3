package SSL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class Cliente{
	
	public static void main(String[] args) {
		try {
			SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
			SSLSocket socket = (SSLSocket) ssf.createSocket();
			
			InetSocketAddress addr = new InetSocketAddress("localhost", 3333);
			socket.connect(addr);
			
			OutputStream os = socket.getOutputStream();
			String mensaje = "Mensaje desde el cliente por SSL";
			os.write(mensaje.getBytes());
			
			socket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
