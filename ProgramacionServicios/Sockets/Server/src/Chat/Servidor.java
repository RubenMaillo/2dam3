package Chat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Servidor extends Thread{
	
	public static List<Socket> clientes = new ArrayList<Socket>();
	Socket con;
	
	public Servidor(Socket con) {
		this.con = con;
	}

	public static void main(String[] args) {
		try {
			iniciar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public static void iniciar() throws IOException {
		ServerSocket server = new ServerSocket(2222);
		while(true) {
			try {
				Socket client = server.accept();
				clientes.add(client);
				new Cliente(client).start();
				
				
			
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
		
		server.close();
		
	}
	
	/*public void run() {
		
		try {
			
			System.out.println("Cliente conectado --> " + con.getRemoteSocketAddress().toString());
			
			BufferedWriter bw1 = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
			bw1.write("Conectado\n");
			bw1.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String linea = null;
			linea = br.readLine();
			
		
			while(!linea.equalsIgnoreCase("END")) {
				broadcast(con, linea);
				linea = br.readLine();
			}
			
			cerrarConexion(con);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	public static void broadcast(Socket con, String linea) {
		System.out.println(clientes.size());
		try {
			for(Socket sock : clientes) {
				if(!(sock.getRemoteSocketAddress().toString()).equalsIgnoreCase(con.getRemoteSocketAddress().toString())) {
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
					System.out.println("Cliente " + sock.getRemoteSocketAddress().toString() + ": "+linea);
					bw.write(sock.getRemoteSocketAddress().toString() + ": " + linea +"\n");
					bw.flush();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public static void cerrarConexion(Socket con) {
		clientes.remove(con);
		System.out.println("Conexi�n cerrada con el cliente " + con.getRemoteSocketAddress().toString() + "\n");
	}*/

}