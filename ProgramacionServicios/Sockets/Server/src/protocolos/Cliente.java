package protocolos;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Cliente{
	
	public static void main(String[] args) {
		iniciar();
	}
	
	public static void iniciar() {
		try {
			
			Scanner sc = new Scanner(System.in);
			
			Socket clientSocket = new Socket();
			InetSocketAddress isa = new InetSocketAddress("localhost", 3333);
			clientSocket.connect(isa);
			
			InputStream is = clientSocket.getInputStream();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			new Escucha(br).start();
			
			System.out.println(br.readLine());
			
			OutputStream os = clientSocket.getOutputStream();
			
			String expresion = "";
			String mensaje = "";
			
			while(!mensaje.equalsIgnoreCase("END")) {
								
				System.out.print("Escribe una expresion --> ");
				
				if((expresion = sc.nextLine()).equalsIgnoreCase("end")) {
					clientSocket.close();
					break;
				}
				else
					expresion += "\n";
			
				os.write(expresion.getBytes());
				
				System.out.print("Escribe el mensaje --> ");
				
				mensaje = sc.nextLine() + "\n";
				
				os.write(mensaje.getBytes());
								
				os.flush();
								
			}
			
			clientSocket.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static class Escucha extends Thread {
		
		BufferedReader br;
		
		public Escucha(BufferedReader br) {
			this.br = br;
		}
		
		
		public void run() {
			try {
				String resultado;
			
				if((resultado = br.readLine()) !=  null) {
					System.out.println("RESULTADO --> " + resultado);
					//resultado = br.readLine();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
}
