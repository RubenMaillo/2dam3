package protocolos;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Servidor extends Thread{
	
	private static Map<String, String> mapa = new HashMap<String, String>();
	Socket con;
	
	public Servidor(Socket con) {
		this.con = con;
	}

	public static void main(String[] args) {
		try {
			iniciar();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	public static void iniciar() throws IOException {
		ServerSocket server = new ServerSocket(3333);
		while(true) {
			try {
			
				Socket client = server.accept();
				System.out.println("alguien se ha conectado");
				
				new Servidor(client).start();
			
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
			
		}
		
		server.close();
		
	}
	
	public void run() {
		
		try {
			
			System.out.println("Cliente conectado --> " + con.getRemoteSocketAddress().toString());
			
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
			bw.write("Conectado\n");
			bw.flush();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String linea = null;
			linea = br.readLine();
			
			while(!linea.equalsIgnoreCase("END")) {
				
				switch(linea) {
					case "MSG":
						System.out.println("El mensaje es: " + br.readLine());
						break;
						
					case "EXP":
						if(mapa.isEmpty())
							mapa.put("A", br.readLine());
						else
							mapa.put("B", br.readLine());
						
						break;
						
					case "VAL":
						String mensaje = br.readLine();
						mensaje = mensaje.replace("A", "(" + mapa.get("A") + ")");
						mensaje = mensaje.replace("B", "(" + mapa.get("B") + ")");
						
						System.out.println(mensaje);
						ScriptEngineManager mgr = new ScriptEngineManager();
						ScriptEngine engine = mgr.getEngineByName("JavaScript");
						System.out.println("SERVIDOR:\n" + engine.eval(mensaje));
						
						//MANDAR MENSAJE AL CLIENTE
						bw.write(engine.eval(mensaje).toString() + "\n");
						bw.flush();				    
				}
				
				linea = br.readLine();
			}
			
			
			System.out.println("Conexi�n cerrada con el cliente " + con.getRemoteSocketAddress().toString() + "\n");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}


}