
public class Main {
	
	public static void iniciar() {
		// crear barbería con 5 asientos
		Barberia barberia = new Barberia(5);
		
		// Crear barbero y asociarlo a la barberia 
		Barbero barbero = new Barbero(barberia);
		
		// Asociar barbero a la barberia
		barberia.setBarbero(barbero);
		
		//iniciar hilo barbero
		new Thread(barbero).start();
		
		
		// iniciar 15 clientes que esperan en la calle si no logran entrar.
		for (int i = 0; i < 15; i++) {
			new Thread(new Cliente(""+i,barberia)).start();
		}		
	}
	
	public static void main(String[] args) {
		iniciar();
	}
}
