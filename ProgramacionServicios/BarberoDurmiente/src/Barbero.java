
public class Barbero implements Runnable {
	
	private Barberia barberia;
	private final int tiempoDeAfeitado = 5000;
	private boolean dormido;
	
	
	public Barbero(Barberia b) {
		barberia = b;	
		
		// al principio, el barbero está dormido
		dormido = true;
	}
	
	public void dormir() {
		// función que duerme al barbero hasta que entre alguien en la barbería
		synchronized(this) {
			try {
				System.out.println("El barbero se duerme");
				dormido = true;
				wait();
				System.out.println("El barbero se ha despertado!");

			} catch (InterruptedException e) {
			}
		}
	}
	
	
	@Override
	public void run() {
		while (true) {			
			if (barberia.estaVacia()) {
				dormir();
			}
			Cliente c = llamarCliente();
			afeitarCliente(c);
		}
	}
	
	public synchronized Cliente llamarCliente() {
		Cliente c = barberia.primerCliente();
		synchronized(c) {
			System.out.println("El barbero llama al cliente "+ c.getNombre() );
			c.notify();
		}
		dormido = false; //indica que el barbero entrará en wait pero no porque esté dormido
		
		// el bucle es necesario porque existe la posibilidad de que el barbero 
		// se despierte sin que haya llegado el cliente al sillón de afeitado (p.e entra un cliente a la barberia)
		// con esto nos aseguramos de que cuando se vuelva activo, el cliente al que esperamos estará sentado en el 
		// sillón de afeitado
		
		while (!dormido && c != barberia.getSillonAfeitado().getCliente()) {
			System.out.println("barbero esperando a que cliente "+c.getNombre()+" se siente en el sillon");
			synchronized(this) {
				try {
					// esperar a que el cliente llegue al sillon de afeitado
					wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
		return c;		
	}
	
	public void afeitarCliente(Cliente c) {
		
		try {
			System.out.println("El barbero está afeitando al cliente "+c.getNombre());
			Thread.sleep(tiempoDeAfeitado);
			System.out.println("El barbero terminó con el cliente "+c.getNombre());
			
			// el bucle es para que el barbero espere al cliente a que se vaya (antes de llamar a otro)
			while (c == barberia.getSillonAfeitado().getCliente()) {
				System.out.println("barbero esperando a que el cliente "+c.getNombre() + "abandone el sillon de afeitado");
				synchronized(c) {
					c.notify();
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}	
	}	
}
