import java.util.concurrent.locks.ReentrantLock;

public class SillonAfeitado {
	// cliente que está sentado en el sillón de afeitado
	
	private Cliente cliente;
	
	// no es necesario porque no compiten por el sillón de afeitado
	private ReentrantLock lock;
	
	public SillonAfeitado() {
		lock = new ReentrantLock();		
	}

	public synchronized void sentar(Cliente c) {
		lock.lock();
		cliente = c;
		System.out.println("El cliente "+ c.getNombre()+" se sienta en la sillon de afeitado");
		
	}
	public synchronized void levantar(Cliente c) {
		lock.unlock();
		cliente = null;
		System.out.println("El cliente "+ c.getNombre()+" se levanta del sillon de afeitado");
	}

	public Cliente getCliente() {
		return cliente;
	}
}
