
public class Cliente implements Runnable {
	
	private String nombre;
	private Barberia barberia;
	
	public Cliente(String nombre, Barberia b) {
		barberia = b;
		this.nombre = nombre;
	}
	
	
	
	
	@Override
	public void run() {
		// el cliente entra en la barbería
		barberia.entrar(this);
		
		// el cliente espera sentado (wait) en la barbería a que le llamen
		esperarAtencion();
		
		// el cliente es llamado por el barbero y se sienta en el sillón de afeitado
		// y espera (wait) hasta que el barbero termine su trabajo
		sentarseEnSillonDeAfeitado();
		
		// cuando el barbero termina su trabajo (notify), el cliente se va
		levantarseDelSillonYSalir();
	}
	
	public synchronized void esperarAtencion() {
		
		try {
			wait();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("El cliente "+getNombre()+ " se levanta de la zona de espera");
		
	}
	
	public synchronized void sentarseEnSillonDeAfeitado() {
		// el cliente se sienta en el sillón de afeitado
		barberia.getSillonAfeitado().sentar(this);
		
		try {
				
			synchronized(barberia.getBarbero()) {
				// el cliente notifica al barbero de que ha llegado al sillón de afeitado
				barberia.getBarbero().notify();					
			}
			
			// el cliente espera a que le afeiten la barba
			wait();
						
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void levantarseDelSillonYSalir() {
		System.out.println("El cliente "+nombre+" ha SALIDO de la barberia");		
		barberia.salir(this);
	}
	
	public String getNombre() {
		return nombre;
	}

}
