import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.Semaphore;

public class Barberia {
	
	
	private Queue<Cliente> clientes;
	
	private final int numSillas;
	private final int numClientes = 5;
	
	private Semaphore semaforo = new Semaphore(numClientes);
	
	private Barbero barbero;
	
	private SillonAfeitado sillon;
	
	
	public Barberia(int numSillas) {
		clientes = new LinkedList<Cliente>();
	
		this.numSillas = numSillas; 
		this.barbero = barbero;
		
		sillon = new SillonAfeitado();
	}
	
	
	
	public synchronized void sentarCliente(Cliente c) {
		clientes.add(c);
	}
		

	public void entrar(Cliente c) {
		try {
			semaforo.acquire();
			sentarCliente(c);
			System.out.println("Cliente "+c.getNombre()+" ha ENTRADO en la barberia");

			synchronized(barbero) {
				// el barbero puede estar durmiendo o esperando
				barbero.notify();
			}
			
		} catch (InterruptedException e) { 			
			e.printStackTrace();
		}		
	}
	
	public synchronized void salir(Cliente c) {
		// se levanta del sillon de afeitado
		sillon.levantar(c);
		
		semaforo.release();
		System.out.println("El cliente "+c.getNombre()+" ha salido de la barberia");		
	}

	

	public synchronized Cliente primerCliente() {
		return this.clientes.poll();
	}
	
	public boolean estaVacia() {
		return clientes.isEmpty();
	}
	
	// getters y setters
	
	public SillonAfeitado getSillonAfeitado() {
		return sillon;
	}
	
	public Barbero getBarbero() {
		return barbero;
	}

	public void setBarbero(Barbero b) {
		this.barbero = b;
	}
}
