package BarberoDurmiente;

import java.util.concurrent.Semaphore;

public class Sillon {

	private Cliente cliente;
	private boolean ocupado;
	private Semaphore sem;
	
	public Sillon(Semaphore sem) {
		cliente = null;
		ocupado = false;
		this.sem = sem;
	}
	
	public void sentar() {
		try {
			sem.acquire();
			setCliente(cliente);
			setOcupado(true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void levantar() {
		sem.release();
		setCliente(null);
		setOcupado(false);
	}

	public boolean isOcupado() {
		return ocupado;
	}

	public void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}
	
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	public Cliente getCliente() {
		return cliente;
	}
	
}
