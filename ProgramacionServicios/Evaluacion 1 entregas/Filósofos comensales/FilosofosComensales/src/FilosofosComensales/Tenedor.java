package FilosofosComensales;

import java.util.concurrent.Semaphore;

class Tenedor{
	private int id;
	private Semaphore sem;
	private boolean ocupado;
	
	public Tenedor(int id, Semaphore sem) {
		this.id = id;
		this.sem = sem;
		this.ocupado = false;
	}
	
	public synchronized void cogerTenedor() {
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		setOcupado(true);
	}
	
	public synchronized void dejarTenedor() {
		sem.release();
		setOcupado(false);
	}

	public int getId() {
		return id;
	}

	public Semaphore getSem() {
		return sem;
	}

	public boolean isOcupado() {
		return ocupado;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setSem(Semaphore sem) {
		this.sem = sem;
	}

	public synchronized void setOcupado(boolean ocupado) {
		this.ocupado = ocupado;
	}
}