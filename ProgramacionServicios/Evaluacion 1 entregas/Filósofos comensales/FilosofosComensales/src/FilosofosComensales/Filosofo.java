package FilosofosComensales;

class Filosofo extends Thread{
	private int id;
	private Tenedor izda;
	private Tenedor dcha;
	
	public Filosofo(int id, Tenedor izda, Tenedor dcha) {
		super();
		this.id = id;
		this.izda = izda;
		this.setDcha(dcha);
	}
	
	public void run() {
		while(true) {
			pensando();
			if(izda.isOcupado()==false) {
				izda.cogerTenedor();
				if(dcha.isOcupado()==false) {
					dcha.cogerTenedor();
					comiendo();
					izda.dejarTenedor();
					dcha.dejarTenedor();
				}
				else {
					izda.dejarTenedor();
					pensando();
				}	
			}
			else {
				pensando();
			}
		}
	}

	public synchronized void pensando() {
		try {			
			sleep((long) ((Math.random()*5000)+1));
			FilosofosComensales.mesa[(int) getId()] = ".";
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void comiendo() {
		try {
			int i = 0;
			sleep((long) ((Math.random()*5000)+1));
			FilosofosComensales.mesa[(int) getId()] = "x";
			
			System.out.print("[ | ");
			for(String f : FilosofosComensales.mesa) {
				if((int) getId() == i)
					f = "y";
				System.out.print(f + " | ");
				i++;
			}
			System.out.println("]\n");
			
			pensando();
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Tenedor getIzda() {
		return izda;
	}

	public void setIzq(Tenedor izq) {
		this.izda = izq;
	}

	public Tenedor getDcha() {
		return dcha;
	}

	public void setDcha(Tenedor dcha) {
		this.dcha = dcha;
	}
	
}