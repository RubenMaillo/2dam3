package FilosofosComensales;

import java.util.concurrent.Semaphore;

class FilosofosComensales{
	
	public static String[] mesa = new String[5];

    public static void main(String[] args) {
    	int numFilosofos = 5;
    	
        Filosofo filosofos[] = new Filosofo[numFilosofos];
        Tenedor tenedores[] = new Tenedor[numFilosofos];

        for(int i = 0; i < tenedores.length; i++){
            tenedores[i] = new Tenedor(i, new Semaphore(1));
        }

        for(int j = 0; j < filosofos.length; j++){
            filosofos[j] = new Filosofo(j, tenedores[j], tenedores[(j+1)%filosofos.length]);
            filosofos[j].start();
        }
        
      //Con esto puedo saber que filosofo tiene que tenedor
        //for (int i = 0; i < filosofos.length; i++) {
        //System.out.println("--------");
        //System.out.println("Filosofo " + filosofos[i].getId());
        //System.out.println(filosofos[i].getDcha().getId());
        //System.out.println(filosofos[i].getIzq().getId());
       // System.out.println("--------");
       //}
    }

}