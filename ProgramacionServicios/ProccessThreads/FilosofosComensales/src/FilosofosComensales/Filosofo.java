package FilosofosComensales;

class Filosofo extends Thread{
	private int id;
	private Tenedor izq;
	private Tenedor dcha;
	
	public Filosofo(int id, Tenedor izq, Tenedor dcha) {
		super();
		this.id = id;
		this.izq = izq;
		this.setDcha(dcha);
	}

	public void run() {
		while(true) {
			pensando();
			//(izq.getSem()).has
			if(izq.isOcupado()==false && dcha.isOcupado()==false) {
				izq.cogerTenedor();
				dcha.cogerTenedor();
				comiendo();
				izq.dejarTenedor();
				dcha.dejarTenedor();
				//System.out.println("Dejando tenedores... "+ izq.getId() + " / " + dcha.getId() + " el filosofo " + getId());
				pensando();
			}
		}
	}
	
	public synchronized void pensando() {
		try {
			//System.out.println("Pensando...");
			sleep((long) ((Math.random()*10000)+1));
			FilosofosComensales.mesa[(int) getId()] = ".";
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public synchronized void comiendo() {
		try {
			//System.out.println("Comiendo " + getId() + " con tenedores " + izq.getId() + " / " + dcha.getId());
			sleep((long) ((Math.random()*10000)+1));
			FilosofosComensales.mesa[(int) getId()] = "x";
			
			System.out.print("[ | ");
			for(String f : FilosofosComensales.mesa) {
				System.out.print(f + " | ");
			}
			System.out.println("]\n");
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Tenedor getIzq() {
		return izq;
	}

	public void setIzq(Tenedor izq) {
		this.izq = izq;
	}

	public Tenedor getDcha() {
		return dcha;
	}

	public void setDcha(Tenedor dcha) {
		this.dcha = dcha;
	}
	
}