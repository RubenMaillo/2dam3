package Thread;

public class Ejemplo1 implements Runnable{

    public void run() {
        System.out.println("Hello from a thread!");
    }
    
    public static void main(String args[]) {
        (new Thread(new Ejemplo1())).start();
    }

}

