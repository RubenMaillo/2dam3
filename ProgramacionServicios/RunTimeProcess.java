import java.io.IOException;
import java.util.Arrays;

class RunTimeProcess {
	public static void main(String[] args) throws Exception{
		if(args.length <= 0){
			System.err.println("Se necestia un programa a ejecutar");
			System.exit(-1);
		}

		RunTime runtime = Runtime.getRuntime();
		try {
            Process process = runtime.exec(args);
            Thread.sleep(5000);
            process.destroy();
		} catch (Exception e) {
			System.err.println("Excepcion de E/S!!");
			System.exit(-1);
		} 
	}
}