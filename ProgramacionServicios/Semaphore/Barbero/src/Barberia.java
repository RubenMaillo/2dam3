import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.util.*;
public class Barberia extends Thread{
	
	private int nSillas;
	private Semaphore semaforo;
	private LinkedList<Cliente> aClientes = new LinkedList<>();
	private Sillon sillon = new Sillon();
	
	public Barberia(int sillas) {
		this.nSillas = sillas;
		this.semaforo = new Semaphore(sillas);
	}
	
	public void entrar(Cliente cliente) throws InterruptedException{
		if(aClientes.size()<nSillas) { // Si no est�n ocupadas todas las sillas de la barber�a
			System.out.println("El cliente n�mero "+cliente.getNumCliente()+" ha entrado en la barber�a");
			sentarEspera(cliente);			
		}else {
			System.out.println("El cliente n�mero "+cliente.getNumCliente()+" ha intentado entrar en la barber�a pero est� llena");
		}
	}
	
	public synchronized void sentarEspera(Cliente cliente) throws InterruptedException {
		System.out.println("El cliente n�mero "+cliente.getNumCliente()+" se ha sentado en un asiento");
		semaforo.acquire();
		aClientes.add(cliente); // Guardo los clientes en un array		
	}
	
	public Cliente clienteSiguente() {
		semaforo.release();
		return aClientes.poll();
	}	

	public void irsePaciencia(Cliente cliente) {
		System.out.println("El cliente " + cliente.getNumCliente() + " se ha cansado de esperar y se ha ido");
		semaforo.release();
		aClientes.remove(cliente);
	}	
	
	public void salir() throws InterruptedException {
		System.out.println("El cliente ha salido de la barber�a");
	}

	public Sillon getSillon() {
		return sillon;
	}

	public void setSillon(Sillon sillon) {
		this.sillon = sillon;
	}

	public LinkedList<Cliente> getClientes() {
		return aClientes;
	}	
}
