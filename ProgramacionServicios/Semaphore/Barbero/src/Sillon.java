import java.util.concurrent.Semaphore;
public class Sillon {
	Boolean sentado = false;
	Cliente cliente;
	Semaphore semaforo;
	
	Sillon(){
		this.sentado=false;
		semaforo = new Semaphore(1);
	}
	
	public void sentar() throws InterruptedException{
		semaforo.acquire();
		sentado = true;
	}
	
	public void levantar(){
		sentado = false;
		semaforo.release();
	}
}
