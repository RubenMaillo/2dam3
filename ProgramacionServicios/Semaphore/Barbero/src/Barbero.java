import java.util.Date;
import java.util.Random;

public class Barbero extends Thread{
	private Boolean dormido;
	private Barberia barberia;
	private Cliente cliente;
	
	Barbero(Barberia barberia){
		this.barberia = barberia;
		this.setDormido(false);
	}	

	public void run() {
		while (true) {	
			try {
				if (barberia.getClientes().size()==0) {
					intentarDormir();
				}else {
					cortarBarba(barberia.clienteSiguente());
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void cortarBarba(Cliente cliente) throws InterruptedException {
		barberia.getSillon().sentar();
		System.out.println("El cliente n�mero " + cliente.getNumCliente() + " se ha sentado en el asiento del barbero y le est� cortando la barba");
		
		sleep(cliente.getPelos());
		System.out.println("El barbero ha terminado de cortar la barba");
		barberia.getSillon().levantar();
		barberia.salir();
	}
	
	public void intentarDormir() throws InterruptedException {
	    Boolean random = new Random().nextBoolean(); // Un random de true o false
	    if(random) {
	    	System.out.println("<- El barbero se ha puesto a dormir");
			this.setDormido(true);
			barberia.getSillon().sentar();
			Thread.sleep(5000);
			despertar();
	    }else {
	    	System.out.println("El barbero ha intentado dormirse");
	    	sleep(1000);// Espera un segundo antes de volver a probar
	    }
	}
	
	public void despertar() throws InterruptedException {
		// Notifico a todos los clientes de que el barbero se ha despertado
        synchronized (this)
        {
        	notify();            
        }
		this.setDormido(false);	
		barberia.getSillon().levantar();
		System.out.println("-> El barbero se ha despertado");
	}

	public Boolean getDormido() {
		return dormido;
	}

	public void setDormido(Boolean dormido) {
		this.dormido = dormido;
	}
	

}
