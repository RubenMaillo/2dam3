
public class crearCliente extends Thread{
	private Barberia barberia;
	private Barbero barbero;
	private int numCliente=0;
	
	crearCliente(Barberia barberia, Barbero barbero) {
		this.barberia = barberia;
		this.barbero = barbero;
	}
	
	public void run() {
		while(true) {
			try {
				numCliente++;				
				Thread cliente = new Thread(new Cliente(barberia, numCliente, barbero));				
				cliente.start();
				sleep(5000); // Creo un nuevo cliente cada 5 segundos
				
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
		}
	}
}
