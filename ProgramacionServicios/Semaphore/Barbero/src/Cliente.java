import java.util.concurrent.TimeUnit;

public class Cliente extends Thread{
	
	private Barberia barberia;	
	private Barbero barbero;
	private int numCliente; // Esto tambi�n va a servir para saber el turno de cada uno
	private int paciencia;
	private int pelos;
	
	Cliente(Barberia barberia, int numCliente, Barbero barbero){
		this.barbero = barbero;
		this.barberia = barberia;
		this.numCliente = numCliente;
		this.pelos = (int) ((Math.random()*((10000-1)+1))+1); // Valor de espera al cortar el pelo random
		this.paciencia = 5000;
		//this.paciencia = (int) ((Math.random()*((10000-1)+1))+1);
	}
	
	public void run() {		
			try {				
				barberia.entrar(this);
				if(barbero.getDormido()) {
					iniciarPaciencia();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} 
	}
	
	public void iniciarPaciencia() throws InterruptedException{
        synchronized (this)
        {
            try{
                this.wait(getPaciencia());
            } 
            catch (InterruptedException e1){
            }
        }		
		barberia.irsePaciencia(this);
	}

	public int getNumCliente() {
		return numCliente;
	}
	
	public int getPaciencia() {
		return paciencia;
	}
	
	public int getPelos() {
		return pelos;
	}

}
