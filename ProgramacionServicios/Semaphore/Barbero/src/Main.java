
public class Main {
	public static String[] asientosEspera = new String[5];
	public static String[] asientoBarbero = new String[2];	
	
	public static void main(String[] args) {
		
		for (int i = 0; i < asientosEspera.length; i++) {
			asientosEspera[i] = "X";
		}
		
		for (int i = 0; i < asientoBarbero.length; i++) {
			asientoBarbero[i] = "X";
		}
		
		int nSillas = 5;
		Barberia barberia = new Barberia(nSillas);
		Barbero barbero = new Barbero(barberia);
		crearCliente crearCliente = new crearCliente(barberia, barbero);
		
		barbero.start();
		crearCliente.start();
	}
}