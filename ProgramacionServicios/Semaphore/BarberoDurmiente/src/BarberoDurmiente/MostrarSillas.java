package BarberoDurmiente;

public class MostrarSillas {

	public synchronized static void mostrarEstado(Barberia barberia) {
		
		Main.sEspera[0] = ".";
		Main.sEspera[1] = ".";
		Main.sEspera[2] = ".";
		Main.sEspera[3] = ".";
		Main.sEspera[4] = ".";
		
		Main.sBarbero[0] = ".";
		Main.sBarbero[1] = "B";
		
		
		if(barberia.getClientes().size() != 0)
			for(int i = 0; i < barberia.getClientes().size(); i++) {
				Main.sEspera[i] = "x";
			}
		
		if(barberia.getSillon().isOcupado()) {
			if(barberia.getBarbero().isDurmiendo()) {
				Main.sBarbero[0] = "B";
				Main.sBarbero[1] = ".";			
			}
			else {
				Main.sBarbero[0] = "x";
				Main.sBarbero[1] = "B";
			}
		}
		
		System.out.print(" | ");
		for(String sE : Main.sEspera) {
			System.out.print(sE + " | ");
		}
		
		System.out.print("  -----  | ");
		
		for(String sB : Main.sBarbero) {
			System.out.print(sB + " | ");
		}
		
		System.out.println("");
	}
	
}
