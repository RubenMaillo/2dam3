package BarberoDurmiente;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class Main {
	private static int numSillas = 5;
	public static String[] sEspera = new String[numSillas];
	public static String[] sBarbero = new String[2];
	
	public static void main(String[] args) {
		
		int i = 0;
		Sillon sillon = new Sillon(new Semaphore(1));
		Barberia barberia = new Barberia(numSillas, sillon);
		Barbero barbero = new Barbero(barberia);
		
		barberia.setBarbero(barbero);
		Cliente cliente;
		
		for(String sE : sEspera) {
			sE = ".";
		}
		
		sBarbero[0] = "B";
		sBarbero[1] = ".";
		
		barbero.start();
		
		while(true) {
			try {
				cliente = new Cliente(i, barberia, barbero);
				cliente.sleep((long) ((Math.random()*10000)+1));
				cliente.start();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			i++;
		}
	}
}
