package BarberoDurmiente;

import java.util.LinkedList;
import java.util.concurrent.Semaphore;

public class Barberia {
	
	private int numSillas;
	private Sillon sillon;
	private Barbero barbero;
	private Semaphore sem;
	private LinkedList<Cliente> clientes = new LinkedList<Cliente>();
	
	public Barberia(int numSillas, Sillon sillon) {
		this.numSillas = numSillas;
		this.sillon = sillon;
		this.sem = new Semaphore(numSillas);
	}
	
	public void entrar(Cliente cliente) {
		//Si hay asientos libres se sienta a esperar
		if(clientes.size() < numSillas) {
			sentarse(cliente);
		}
		//Sino se va
		else
			System.out.println("Barberia llena. el cliente " + cliente.getCliente() + " se va.");
	}
	
	public void irse(Cliente cliente) {
		System.out.println("El cliente " + cliente.getCliente() + " se ha ido de la barberia.");
	}
	
	
	public void sentarse(Cliente cliente) {
		try {
			//System.out.println("El cliente " + cliente.getCliente() + " se sienta a esperar.");
			clientes.add(cliente); //A�ado el cliente a la cola
			sem.acquire();
			MostrarSillas.mostrarEstado(this);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void agotarPaciencia(Cliente cliente) {
		System.out.println("El cliente " + cliente.getCliente() + " ha agotado su paciencia y se va.");
		sem.release();
		clientes.remove(cliente);
		MostrarSillas.mostrarEstado(this);
	}
	
	public Cliente siguiente() {
		sem.release();
		return clientes.poll();
	}

	public Sillon getSillon() {
		return sillon;
	}

	public Barbero getBarbero() {
		return barbero;
	}
	
	public void setBarbero(Barbero barbero) {
		this.barbero = barbero;
	}

	public LinkedList<Cliente> getClientes() {
		return clientes;
	}
	
	
}
