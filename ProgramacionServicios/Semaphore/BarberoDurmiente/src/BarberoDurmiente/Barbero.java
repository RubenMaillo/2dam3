package BarberoDurmiente;

import java.util.Random;

public class Barbero extends Thread {

	private boolean durmiendo;
	private Barberia barberia;
	
	public Barbero(Barberia barberia) {
		setDurmiendo(false);
		this.barberia = barberia;
	}
	
	public void run() {
		while(true) {
			if(barberia.getClientes().size() == 0)
				intentarDormir();
			else
				cortarBarba(barberia.siguiente());
		}
	}
	
	public void intentarDormir() {
		Boolean rDormir = new Random().nextBoolean();
		
		if(rDormir) {
			
			try {
				//System.out.println("--> El barbero se ha dormido");
				setDurmiendo(true);
				barberia.getSillon().sentar();
				MostrarSillas.mostrarEstado(barberia);
				sleep((long) ((Math.random()*10000)+1));
				despertarse();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
		}
		else {
			try {
				sleep((long) ((Math.random()*10000)+1));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void despertarse() {
		setDurmiendo(false);
		barberia.getSillon().levantar();
		
		for(Cliente cliente : barberia.getClientes())
			synchronized(cliente) {
				cliente.notify();
			}
		
		MostrarSillas.mostrarEstado(barberia);
	}
	
	public void cortarBarba(Cliente cliente) {
		try {
			barberia.getSillon().sentar();
			MostrarSillas.mostrarEstado(barberia);
			
			sleep((long) ((Math.random()*10000)+1));
			
			barberia.getSillon().levantar();
			barberia.irse(cliente);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public boolean isDurmiendo() {
		return durmiendo;
	}

	public void setDurmiendo(boolean durmiendo) {
		this.durmiendo = durmiendo;
	}

}
