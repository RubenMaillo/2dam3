package BarberoDurmiente;

public class Cliente extends Thread{

	private int cliente;
	private Barberia barberia;
	private Barbero barbero;
	private long paciencia;
	
	public Cliente(int cliente, Barberia barberia, Barbero barbero) {
		this.cliente = cliente;
		this.barberia = barberia;
		this.barbero = barbero;
		this.paciencia = (long) ((Math.random()*5000)+1); //si el barbero duerme el cliente espera este tiempo a que se despierte
	}
	
	public void run() {
		barberia.entrar(this);
		//si el barbero duerme el cliente espera
		if(barbero.isDurmiendo())
			esperarPaciencia(this);
	}
	
	public void esperarPaciencia(Cliente cliente) {
		synchronized (cliente) {
			try {
				cliente.wait(paciencia);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			//si el barbero no se despierta el cliente se va
			if(barbero.isDurmiendo())
				barberia.agotarPaciencia(this);
		}
		
	}
		

	public int getCliente() {
		return cliente;
	}

	public long getPaciencia() {
		return paciencia;
	}
}
