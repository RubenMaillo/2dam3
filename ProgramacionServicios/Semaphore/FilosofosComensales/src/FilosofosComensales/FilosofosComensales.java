package FilosofosComensales;

import java.util.concurrent.Semaphore;

class FilosofosComensales{
	
	public static String[] mesa = new String[5];

    public static void main(String[] args) {
    	int numFilosofos = 5;
    	
        Filosofo filosofos[] = new Filosofo[numFilosofos];
        Tenedor tenedores[] = new Tenedor[numFilosofos];
        
        for(int i = 0; i < numFilosofos; i++) {
        	mesa[i] = ".";
        }

        for(int i = 0; i < tenedores.length; i++){
            tenedores[i] = new Tenedor(i, new Semaphore(1));
        }

        for(int j = 0; j < filosofos.length; j++){
            filosofos[j] = new Filosofo(j, tenedores[j], tenedores[(j+1)%filosofos.length]);
            filosofos[j].start();
        }
        
        for(int j = 0; j < filosofos.length; j++){
            try {
				filosofos[j].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }

}