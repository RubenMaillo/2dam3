package ch.makery.login.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Usuarios {
	
	private final StringProperty usuario;
	private final StringProperty contrasena;
	
	public Usuarios() {
        this(null, null);
    }

	public Usuarios(String usuario, String contrasena) {
		this.usuario = new SimpleStringProperty(usuario);
		this.contrasena = new SimpleStringProperty(contrasena);
	}

	public StringProperty getUsuario() {
		return usuario;
	}

	public StringProperty getContrasena() {
		return contrasena;
	}
	
}
