package ch.makery.login.view;

import java.util.Optional;

import ch.makery.login.Main;
import ch.makery.login.model.Usuarios;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

public class LoginController {
	
	@FXML
    private TextField user;
    @FXML
    private TextField pass;
    
    private ObservableList<Usuarios> userData = FXCollections.observableArrayList();

    public LoginController() {
        userData.add(new Usuarios("admin", "admin"));
        userData.add(new Usuarios("Ruben", "123"));
    }
	/**
	 * Called when the user clicks on the delete button.
	 */
	@FXML
	private void handleAceptar() {
		
		boolean correcto = false;
		
		//si el inicio de sesion es correcto
		for(Usuarios usuario : userData) {
			if((user.getText()).equals(usuario.getUsuario().getValue()) && (pass.getText()).equals(usuario.getContrasena().getValue())) {
				correcto = true;
				System.out.println();
			}
		}
		
		if(correcto) {
			//si el inicio de sesion es incorrecto
		    Alert alert1 = new Alert(Alert.AlertType.INFORMATION);
		    alert1.setTitle("Sesion");
		    alert1.setHeaderText(null);
		    alert1.setContentText("Ha iniciado sesi�n correctamente.");
		    alert1.getButtonTypes().clear();
		    ButtonType boutonOk = new ButtonType("Ok");
		    alert1.getButtonTypes().add(boutonOk);
		    Optional<ButtonType> result = alert1.showAndWait();
		    
		    correcto = false;
		}
		else {
			//si el inicio de sesion es incorrecto
		    Alert alert2 = new Alert(Alert.AlertType.INFORMATION);
		    alert2.setTitle("Sesion");
		    alert2.setHeaderText(null);
		    alert2.setContentText("Las credenciales no son correctas.");
		    alert2.getButtonTypes().clear();
		    ButtonType boutonOk2 = new ButtonType("Ok");
		    alert2.getButtonTypes().add(boutonOk2);
		    Optional<ButtonType> result2 = alert2.showAndWait();
		}
	}
	
}
