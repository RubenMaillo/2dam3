package Clases;

import java.sql.Date;

public class Reserva {

	private int usuario, mesa;
	private String fecha;
	private String hora;
	
	public Reserva(int usuario, int mesa, String fecha, String hora) {
		super();
		this.usuario = usuario;
		this.mesa = mesa;
		this.fecha = fecha;
		this.hora = hora;
	}

	public Reserva(int mesa, String fecha, String hora) {
		super();
		this.mesa = mesa;
		this.fecha = fecha;
		this.hora = hora;
	}

	public int getUsuario() {
		return usuario;
	}

	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}
	
	public int getMesa() {
		return mesa;
	}
	
	public void setMesa(int mesa) {
		this.mesa = mesa;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}
	
}
