package Clases;

public class Cesta {
	
	private String nomArt;
	private int unidades;
	private double precio;
	
	public Cesta(String nomArt, int unidades, double precio) {
		super();
		this.nomArt = nomArt;
		this.unidades = unidades;
		this.precio = precio;
	}

	public String getNomArt() {
		return nomArt;
	}

	public void setNomArt(String nomArt) {
		this.nomArt = nomArt;
	}

	public int getUnidades() {
		return unidades;
	}

	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}
