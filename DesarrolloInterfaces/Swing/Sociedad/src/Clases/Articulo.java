package Clases;

public class Articulo {
	
	private String nombre, categoria;
	private int unidades;
	private double precio;
	private int minUnidades, id;
	
	public Articulo(String nombre, String categoria, double precio) {
		super();
		this.nombre = nombre;
		this.categoria = categoria;
		this.precio = precio;
	}

	public Articulo(String nombre, String categoria, int unidades, double precio) {
		super();
		this.nombre = nombre;
		this.categoria = categoria;
		this.unidades = unidades;
		this.precio = precio;
	}
	
	public Articulo(int id, String nombre, String categoria, int unidades, double precio, int minUnidades) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.categoria = categoria;
		this.unidades = unidades;
		this.precio = precio;
		this.minUnidades = minUnidades;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getCategoria() {
		return categoria;
	}
	
	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}
	
	public int getUnidades() {
		return unidades;
	}
	
	public void setUnidades(int unidades) {
		this.unidades = unidades;
	}
	
	public double getPrecio() {
		return precio;
	}
	
	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getMinUnidades() {
		return minUnidades;
	}

	public void setMinUnidades(int minUnidades) {
		this.minUnidades = minUnidades;
	}
	
}
