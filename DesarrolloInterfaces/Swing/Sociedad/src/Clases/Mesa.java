package Clases;

public class Mesa {

	private int id, comensales;

	public Mesa(int id, int comensales) {
		super();
		this.id = id;
		this.comensales = comensales;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getComensales() {
		return comensales;
	}

	public void setComensales(int comensales) {
		this.comensales = comensales;
	}
	
}
