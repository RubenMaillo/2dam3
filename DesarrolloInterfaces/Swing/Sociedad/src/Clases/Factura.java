package Clases;

import java.time.LocalDateTime;
import java.util.List;

public class Factura {

	private int id;
	private int usuario;
	private LocalDateTime fecha;
	private List<Cesta> articulos;
	
	public Factura(int usuario, LocalDateTime fecha) {
		super();
		this.usuario = usuario;
		this.fecha = fecha;
	}
	
	public Factura(int id, int usuario, LocalDateTime fecha, List<Cesta> articulos) {
		super();
		this.id = id;
		this.usuario = usuario;
		this.fecha = fecha;
		this.articulos = articulos;
	}

	public int getUsuario() {
		return usuario;
	}

	public void setUsuario(int usuario) {
		this.usuario = usuario;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Cesta> getArticulos() {
		return articulos;
	}

	public void setArticulos(List<Cesta> articulos) {
		this.articulos = articulos;
	}
	
	
		
}
