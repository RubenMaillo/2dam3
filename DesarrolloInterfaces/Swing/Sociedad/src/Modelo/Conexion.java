package Modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexion {
	
	public static Connection conexion = null;

	public static Connection abrirConexion() {
		Connection conexion = null;
		String url = "jdbc:mysql://localhost/sociedad";
		
		try {
			conexion = DriverManager.getConnection(url, "root", "root");
			System.out.println("Conexion establecida correctamente");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return conexion;
	}
	
	
	public static void cerrarConexion(Connection conexion) {
		try {
			if(conexion != null)
				conexion.close();
		}catch(SQLException ex) {
			System.out.println(ex.getMessage());
		}
	}
}
