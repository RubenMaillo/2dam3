package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Clases.Usuario;

public class ModUsuario {
	
	static Conexion conexion = null;

	public static Usuario findUsuario(String user, String pass) {
		Connection con = conexion.abrirConexion();
		
		Usuario usu = null;
		boolean encontrado = false; 

		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM usuarios as u WHERE user='" + user + "' AND pass='" + pass + "';";
			ResultSet rs = stat.executeQuery(consulta);
			
			if(rs.next()) {
				usu = new Usuario(rs.getInt("u.id"), rs.getString("u.nombre"), rs.getString("u.apellidos"), rs.getString("u.user"), rs.getInt("u.pass"));
				encontrado = true;
			}

		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
		
		return usu;
		
	}
	
}
