package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Clases.Cesta;
import Clases.Reserva;
import Clases.Usuario;

public class ModCesta {
	
	static Conexion conexion = null;

	public static void addCuenta(ArrayList<Cesta> cesta) {
		Connection con = conexion.abrirConexion();
		
		int idFac = ModFactura.selecMaxId();
		int idArt;
		
		try {
			for(Cesta c : cesta) {
				
				if((idArt = existeArticulo(c.getNomArt())) != 0) {
					
					System.out.println(idFac + " / " + idArt + " / " + c.getUnidades());
					
					String consulta = "INSERT INTO cesta(idFactura, idArticulo, unidades) VALUES(?, ?, ?);";
					
					PreparedStatement stat = con.prepareStatement(consulta);					
					stat.setInt(1, idFac);
					stat.setInt(2, idArt);
					stat.setInt(3, c.getUnidades());
					stat.executeUpdate();
					
					String consulta2 = "SELECT * FROM articulos as a WHERE id ='" + idArt + "';";
					ResultSet rs = stat.executeQuery(consulta2);
					rs.next();
					
					int unidades = rs.getInt("a.stock") - c.getUnidades();
					System.out.println(unidades);
					
					String consulta3 = "UPDATE articulos SET stock = ? WHERE id = ?;";
					PreparedStatement stat2 = con.prepareStatement(consulta3);
					stat2.setInt(1, unidades);
					stat2.setInt(2, idArt);
					stat2.executeUpdate();
				}
			}
			
			
		}catch(SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
		
	}
	
	public static int existeArticulo(String art) {
		Connection con = conexion.abrirConexion();
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM articulos as a WHERE nombre='" + art + "';";
			ResultSet rs = stat.executeQuery(consulta);
			
			if(rs.next())
				System.out.println(rs.getInt("a.id"));
				return rs.getInt("a.id");
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return 0;
	}
	
}
