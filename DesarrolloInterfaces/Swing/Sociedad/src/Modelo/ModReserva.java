package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Clases.Mesa;
import Clases.Reserva;

public class ModReserva {
	
	static Conexion conexion = null;
	
	public static Boolean existeReserva(Reserva res) {
		Connection con = conexion.abrirConexion();
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM reservas WHERE id_mesa=" + res.getMesa() + " AND fecha='" + res.getFecha() + "' AND hora='" + res.getHora() + "';";
			ResultSet rs = stat.executeQuery(consulta);
			
			if(rs.next())
				return true;
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return false;
	}

	public static Boolean addReserva(Reserva res) {
		Connection con = conexion.abrirConexion();
		
		if(!existeReserva(res))
		
			try {
				
				String consulta = "INSERT INTO reservas(id_usuario, id_mesa, fecha, hora) VALUES(?, ?, ?, ?);";
				
				PreparedStatement stat = con.prepareStatement(consulta);
				stat.setInt(1, res.getUsuario());
				stat.setInt(2, res.getMesa());
				stat.setString(3, res.getFecha());
				stat.setString(4, res.getHora());
				stat.executeUpdate();
				
				return true;
				
			}catch(SQLException e) {
				System.out.println("Error: " + e.getMessage());
			}finally {
				conexion.cerrarConexion(con);
			}
		
		return false;
	}
	
	public static ArrayList<Reserva> selecReservas() {
		Connection con = conexion.abrirConexion();
		
		ArrayList<Reserva> reservas = new ArrayList<Reserva>();
		
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM reservas as r;";
			ResultSet rs = stat.executeQuery(consulta);
			
			while(rs.next()) {

				int idMesa = rs.getInt("r.id_mesa");
				String fecha = rs.getString("r.fecha");
				String hora = rs.getString("r.hora");
				
				reservas.add(new Reserva(idMesa, fecha, hora));
			}
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return reservas;
	}
	
}
