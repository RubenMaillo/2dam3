package Modelo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Clases.Articulo;
import Clases.Cesta;

public class ModArticulo {
	static Conexion conexion = null;

	public static ArrayList<Articulo> selecArticulos() {
		Connection con = conexion.abrirConexion();
		
		ArrayList<Articulo> articulos = new ArrayList<Articulo>();
		
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM articulos as a;";
			ResultSet rs = stat.executeQuery(consulta);
			Statement stat2 = con.createStatement();
			while(rs.next()) {

				String nombre = rs.getString("a.nombre");
				Double precio = rs.getDouble("a.precio");
				
				String consulta2 = "SELECT * FROM categorias as c where id = " + rs.getInt("a.id_categoria") + ";";
				ResultSet rs2 = stat2.executeQuery(consulta2);
				
				String nombreC = "";
				
				while(rs2.next()) {
					nombreC = rs2.getString("c.nombre");
				}
				
				articulos.add(new Articulo(nombre, nombreC, precio));
			}
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return articulos;
	}
	
	public static ArrayList<Articulo> selecArticulosInforme() {
		Connection con = conexion.abrirConexion();
		
		ArrayList<Articulo> articulos = new ArrayList<Articulo>();
		
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM articulos as a;";
			ResultSet rs = stat.executeQuery(consulta);
			Statement stat2 = con.createStatement();
			while(rs.next()) {

				String nombre = rs.getString("a.nombre");
				Double precio = rs.getDouble("a.precio");
				int uds = rs.getInt("a.stock");
				int minUds = rs.getInt("a.stockMinimo");
				int id = rs.getInt("a.id");
				
				String consulta2 = "SELECT * FROM categorias as c where id = " + rs.getInt("a.id_categoria") + ";";
				ResultSet rs2 = stat2.executeQuery(consulta2);
				
				String nombreC = "";
				
				while(rs2.next()) {
					nombreC = rs2.getString("c.nombre");
				}
				
				articulos.add(new Articulo(id, nombre, nombreC, uds, precio, minUds));
			}
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return articulos;
	}
}
