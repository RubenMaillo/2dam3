package Modelo;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import Clases.Cesta;
import Clases.Factura;
import Clases.Reserva;
import Clases.Usuario;

public class ModFactura {

	static Conexion conexion = null;
	
	public static Boolean addFactura(Usuario user) {
		Connection con = conexion.abrirConexion();
				
		try {
			
			String consulta = "INSERT INTO factura VALUES(null, ?, ?);";
			
			PreparedStatement stat = con.prepareStatement(consulta);
			stat.setInt(1, user.getId());
			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			stat.executeUpdate();
			
			return true;
			
		}catch(SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
		
		return false;
	}
	
	public static int selecMaxId() {
		Connection con = conexion.abrirConexion();
		int id = 0;
		
		try {
			
			Statement stat = con.createStatement();
			String consulta = "SELECT id from factura as f ORDER BY id DESC LIMIT 1;";
			ResultSet rs = stat.executeQuery(consulta);
			
			if(rs.next()) {
				id = rs.getInt("f.id");
				System.out.println("id: " + id);
				return id;
			}
			
		}catch(SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
		
		return id;
	}
	
	public static List<Factura> selecFacturas(String fecha1, String fecha2, Usuario user) throws ParseException {
		Connection con = conexion.abrirConexion();
		
		List<Factura> facturas = new ArrayList<Factura>();
		List<Cesta> cesta = new ArrayList<Cesta>();

		fecha1 = fecha1.replaceAll("/", "-");
		fecha2 = fecha2.replaceAll("/", "-");
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		java.util.Date parsedDate1 = dateFormat.parse(fecha1 + " 00:00:00");
	    Timestamp timestamp1 = new java.sql.Timestamp(parsedDate1.getTime());
	    
	    java.util.Date parsedDate2 = dateFormat.parse(fecha2 + " 23:59:59");
	    Timestamp timestamp2 = new java.sql.Timestamp(parsedDate2.getTime());
		
		try {
			
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM factura as f WHERE idUsuario = " + user.getId() + " AND fecha BETWEEN '" + timestamp1 + "' AND '" + timestamp2 + "';";
			ResultSet rs = stat.executeQuery(consulta);
			
			while(rs.next()) {
				cesta = new ArrayList<Cesta>();
				Statement stat2 = con.createStatement();
				String consulta2 = "SELECT * FROM cesta as c WHERE idFactura = " + rs.getInt("f.id") + ";";
				ResultSet rs2 = stat2.executeQuery(consulta2);
				
				while(rs2.next()) {
					Statement stat3 = con.createStatement();
					String consulta3 = "SELECT * FROM articulos as a WHERE id = " + rs2.getInt("c.idArticulo") + ";";
					ResultSet rs3 = stat3.executeQuery(consulta3);
					
					if(rs3.next()) {
						cesta.add(new Cesta(rs3.getString("a.nombre"), rs2.getInt("c.unidades"), rs3.getDouble("a.precio")));
					}
				}
				
				facturas.add(new Factura(rs.getInt("f.id"), user.getId(), rs.getTimestamp("f.fecha").toLocalDateTime(), cesta));
				
			}
			
		}catch(SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
		
		return facturas;
	}
	
}
