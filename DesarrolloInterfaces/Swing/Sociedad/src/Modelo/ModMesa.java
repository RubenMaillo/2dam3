package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Clases.Articulo;
import Clases.Mesa;

public class ModMesa {

	static Conexion conexion = null;

	public static ArrayList<Mesa> selecMesas() {
		Connection con = conexion.abrirConexion();
		
		ArrayList<Mesa> mesas = new ArrayList<Mesa>();
		
		try {
			Statement stat = con.createStatement();
			String consulta = "SELECT * FROM mesas as m;";
			ResultSet rs = stat.executeQuery(consulta);
			
			while(rs.next()) {

				int id = rs.getInt("m.id");
				int comensales = rs.getInt("m.comensales");
				
				mesas.add(new Mesa(id, comensales));
			}
			
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}finally {
			conexion.cerrarConexion(con);
		}
	
		return mesas;
	}
	
}
