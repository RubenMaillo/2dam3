package Sociedad;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

import Clases.Reserva;
import Clases.Usuario;
import Modelo.ModUsuario;

public class Login extends JFrame{
	
	//private JLabel user, pass;
	private JLabel user, pass;
	private JButton aceptar, borrar;
	private JButton n0, n1, n2, n3, n4, n5, n6, n7, n8, n9, n10, n11;
	private String u="", p="", pas="";
	
	private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
	static List<Reserva> reservas = new ArrayList<Reserva>();
	
	
	public Login() {
		setSize(380, 250);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		GridLayout gl0 = new GridLayout(1, 2);
		cp.setLayout(gl0);
		
		JPanel sesion = new JPanel();
		GridLayout gl = new GridLayout(2, 1);
		
		
		JPanel numeros = new JPanel();
		GridLayout gl2 = new GridLayout(4, 3);
		gl2.setVgap(5);
		gl2.setHgap(5);
		numeros.setLayout(gl2);
		
		n0 = new JButton("0"); n1 = new JButton("1"); n2 = new JButton("2");
		n3 = new JButton("3"); n4 = new JButton("4"); n5 = new JButton("5");
		n6 = new JButton("6"); n7 = new JButton("7"); n8 = new JButton("8");
		n9 = new JButton("9"); n10 = new JButton("-"); n11 = new JButton("-");
		
		JButton[] num = new JButton[] {
			n7, n8, n9, n4, n5, n6, n1, n2, n3, n10, n0, n11
		};
		
		for(JButton boton : num) {
			numeros.add(boton);
			boton.addActionListener(new Sesion());
		}
		
		n10.setVisible(false);
		n11.setVisible(false);
		cp.add(sesion);
		sesion.setLayout(null);
		
		user = new JLabel("--");
		pass = new JLabel("----");
		
		JPanel datos = new JPanel();
		datos.setBounds(27, 50, 128, 36);
		GridLayout gl1 = new GridLayout(2, 2);
		gl1.setVgap(10);
		gl1.setHgap(10);
		datos.setLayout(gl1);
		
		datos.add(new JLabel("Usuario: "));
		datos.add(user);
		datos.add(new JLabel("Contraseņa: "));
		datos.add(pass);
		
		
		sesion.add(datos);
		
		aceptar = new JButton("Aceptar");
		borrar = new JButton("Borrar");
		
		
		JPanel botones = new JPanel();
		botones.setBounds(18, 100, 144, 21);
		GridLayout gl3 = new GridLayout(1, 2);
		gl3.setVgap(10);
		gl3.setHgap(10);
		botones.setLayout(gl3);
		
		botones.add(aceptar);
		botones.add(borrar);
		sesion.add(botones);
		
		aceptar.addActionListener(new IniciarSesion());
		borrar.addActionListener(new Borrar());
		cp.add(numeros);

	}
	
	class Sesion implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JButton b = (JButton) e.getSource();
			
			if(user.getText().equals("") || u.length()<2) {
				u += b.getText();
				user.setText(u);
			} else {
				if(pass.getText().equals("") || p.length()<4) {
					p += b.getText();
					pas += "*";
					pass.setText(pas);
				}
			}
 		}
	 }
	
	class Borrar implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			u = "";
			p = "";
			pas = "";
			
			user.setText("--");
			pass.setText("----");
 		}
	 }
	
	class IniciarSesion implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			Usuario usuario = ModUsuario.findUsuario(user.getText(), p);
			
			if(usuario == null)
				JOptionPane.showMessageDialog(null, "Usuario o contraseņa incorrectos");
			else {
				Inicio vInicio = new Inicio(usuario);
				dispose();
				vInicio.setVisible(true);
				vInicio.setLocationRelativeTo(null);
			}
		}
	 }
}
