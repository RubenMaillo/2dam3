package Sociedad;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class VentanaInforme extends JFrame{
	
	private JTable tabla = new JTable();
	
	private JPanel panelInforme = new JPanel();
	private JScrollPane scrollCuenta = new JScrollPane(panelInforme);
	
	public VentanaInforme(JTable tabla) {
		this.tabla = tabla;
		
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		
		scrollCuenta.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollCuenta.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		
		GridLayout gl = new GridLayout(2, 1);
		gl.setHgap(100);
		panelInforme.setLayout(gl);
		
		JButton print = new JButton("Imprimir PDF");
		
		print.setFont(new Font("Dialog", 0, 14));
		print.setPreferredSize(new Dimension(100, 45));
		
		print.addActionListener(new Impirmir());
		
		panelInforme.add(print);
		panelInforme.add(tabla);
		
		cp.add(scrollCuenta);
		
		
	}
	
	class Impirmir implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			imprimirPDF();
 		}
	}
	
	public void imprimirPDF() {
			try {

			boolean imp = tabla.print();
			
			if(!imp)
				JOptionPane.showMessageDialog(null, "No se puede imprimir la cuenta.");
			else
				JOptionPane.showMessageDialog(null, "Informe impreso con �xito.");
			
		} catch (PrinterException e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		}

}
