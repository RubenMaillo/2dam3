package Sociedad;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import Clases.Mesa;
import Clases.Reserva;
import Clases.Usuario;
import Modelo.ModMesa;
import Modelo.ModReserva;

import javax.swing.JComboBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class VentanaMesas extends JFrame {
	
	private JPanel panelMesas = new JPanel();
	
	private JLabel m;
	
	private JLabel mesa = new JLabel("Mesa seleccionada: ---");
	private int numMesa = 0;
	
	private JDateChooser calendario = new JDateChooser();
	
	private String[] data = {"Comida", "Cena"};
	private JComboBox hora = new JComboBox(data);
	
	private List<Mesa> mesas = new ArrayList<Mesa>();
	private List<Reserva> reservas = new ArrayList<Reserva>();
	
	SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
	
	public VentanaMesas(Usuario user) {
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		Container cp = getContentPane();
		
		
		GridLayout gl = new GridLayout(2, 2);
		panelMesas.setLayout(gl);
		
		
		
		JPanel panelCalendario = new JPanel();
		GridBagLayout gbl_panelCalendario = new GridBagLayout();
		gbl_panelCalendario.columnWidths = new int[]{227, 0};
		gbl_panelCalendario.rowHeights = new int[]{150, 70 ,50 , 50, 0};
		gbl_panelCalendario.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_panelCalendario.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panelCalendario.setLayout(gbl_panelCalendario);
		
		GridBagConstraints gbc_mesa = new GridBagConstraints();
		gbc_mesa.insets = new Insets(0, 0, 5, 0);
		gbc_mesa.gridx = 0;
		gbc_mesa.gridy = 0;
		gbc_mesa.anchor = GridBagConstraints.SOUTH;
		panelCalendario.add(mesa, gbc_mesa);	
		
		GridBagConstraints gbc_calendario = new GridBagConstraints();
		gbc_calendario.insets = new Insets(0, 0, 5, 0);
		gbc_calendario.gridx = 0;
		gbc_calendario.gridy = 1;
		
		calendario.setDate(new Date());
		
		panelCalendario.add(calendario, gbc_calendario);
				
		imprimirMesas(user);
				
		GridBagConstraints gbc_hora = new GridBagConstraints();
		gbc_hora.insets = new Insets(0, 0, 5, 0);
		gbc_hora.gridx = 0;
		gbc_hora.gridy = 2;
		panelCalendario.add(hora, gbc_hora);
		
		JButton guardar = new JButton("Reservar");
		GridBagConstraints gbc_guardar = new GridBagConstraints();
		gbc_guardar.anchor = GridBagConstraints.NORTH;
		gbc_guardar.gridx = 0;
		gbc_guardar.gridy = 3;
		panelCalendario.add(guardar, gbc_guardar);
		
		guardar.addActionListener(new Reservar(user));
		
		cp.add(panelMesas, BorderLayout.CENTER);
		cp.add(panelCalendario, BorderLayout.EAST);
		
		ActionListener cambios = new ActionListener() {//add actionlistner to listen for change
            @Override
            public void actionPerformed(ActionEvent e) {
            	imprimirMesas(user);
            	
            }
		};
		
		
		hora.addActionListener(cambios);
		
		calendario.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
		    public void propertyChange(java.beans.PropertyChangeEvent evt) {
		    	imprimirMesas(user);
		    }
		});
	}
	
	public void selecMesas() {
		mesas = ModMesa.selecMesas();
		reservas = ModReserva.selecReservas();
	}
	
	public void imprimirMesas(Usuario user) {
		
		panelMesas.removeAll();
		
		ImageIcon imgMesa = new ImageIcon("imagenes"+File.separator+"mesa.jpg");
		Image imagen1 = imgMesa.getImage();
		imagen1 = imagen1.getScaledInstance(180, 140, Image.SCALE_SMOOTH);
		imgMesa.setImage(imagen1);
		
		ImageIcon imgMesa2 = new ImageIcon("imagenes"+File.separator+"mesa2.jpg");
		Image imagen2 = imgMesa2.getImage();
		imagen2 = imagen2.getScaledInstance(180, 140, Image.SCALE_SMOOTH);
		imgMesa2.setImage(imagen2);
		
		selecMesas();
		
		for(Mesa ms: mesas) {
			boolean esta = false;
			Reserva reserva = new Reserva(user.getId(), ms.getId(), formateador.format(calendario.getDate()), data[hora.getSelectedIndex()]);
			System.out.println(formateador.format(calendario.getDate()));
			System.out.println(data[hora.getSelectedIndex()]);
			if(ModReserva.existeReserva(reserva)) {
				esta = true;
				m = new JLabel(imgMesa2);
			}
			else 
				m = new JLabel(imgMesa);

			m.setName(String.valueOf(ms.getId()));
			m.setCursor(new Cursor(HAND_CURSOR));
			if(!esta)
				m.addMouseListener(new SeleccionMesa());
			panelMesas.add(m);
		}
		
		panelMesas.revalidate();
		panelMesas.repaint();
	}
	
	class SeleccionMesa extends MouseAdapter {
        public void mouseClicked(MouseEvent e) {
        	JLabel l = (JLabel) e.getSource();
        	numMesa = Integer.parseInt(l.getName());
        	mesa.setText("Mesa seleccionada: " + l.getName());
        }
    }
	
	class Reservar implements ActionListener {
		
		Usuario user;
		
		public Reservar(Usuario user) {
			this.user = user;
		}
		
			
 		public void actionPerformed(ActionEvent e) {
 			boolean existe = false;
 			
 			Reserva reserva = new Reserva(user.getId(), numMesa, formateador.format(calendario.getDate()), data[hora.getSelectedIndex()]);
 			 			
			if(ModReserva.existeReserva(reserva)) {
				existe = true;
			}
			else
				existe = false;
 				
 			
 			System.out.println(existe);
 				
 			if(existe)
 				JOptionPane.showMessageDialog(null, "Ya existe una resera para la mesa, fecha y hora especificadas", "Error", JOptionPane.ERROR_MESSAGE);
 			else {
	 			if(ModReserva.addReserva(reserva))
					JOptionPane.showMessageDialog(null, "Reserva realizada correctamente");
				else
					JOptionPane.showMessageDialog(null, "La reserva no ha podido completarse", "Error", JOptionPane.ERROR_MESSAGE);
 			}			
 		}
	}

}
