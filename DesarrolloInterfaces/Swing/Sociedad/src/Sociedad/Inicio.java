package Sociedad;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.util.ArrayList;

import java.io.File;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;

import Clases.Articulo;
import Clases.Cesta;
import Clases.Reserva;
import Clases.Usuario;
import Modelo.ModArticulo;

public class Inicio extends JFrame{
	
	public static ArrayList<Reserva> reservas = new ArrayList<Reserva>();
	
	private Container cp = getContentPane();
	//private JLabel user, pass;
	private JTextField tuser, tpass;
	private String lUser;
	private JButton aceptar;
	
	private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
	private ArrayList<Articulo> articulos = new ArrayList<Articulo>();
	private ArrayList<Articulo> articulos2 = new ArrayList<Articulo>();
	private ArrayList<Cesta> cesta = new ArrayList<Cesta>();
	
	private int uv=0, ur=0, uc=0, ul=0, i=1;
	
	private GridLayout glCuenta = new GridLayout(2, 3);
	private JTable tablaCuenta;
	private String[] columnas = {"Cant.", "Art�culo", "Precio", ""};
	private DefaultTableModel modelo = new DefaultTableModel(null, columnas);
	
	private JPanel panelCuenta = new JPanel();
	private JScrollPane scrollCuenta = new JScrollPane(panelCuenta);
	
	private Double total = 0.00;
	private JLabel lTotal = new JLabel("Total: " + total + " �");
	
	private ImageIcon imgVino = new ImageIcon("imagenes"+File.separator+"vino.jpg");
	private ImageIcon imgRefresco = new ImageIcon("imagenes"+File.separator+"refresco.jpg");
	private ImageIcon imgCafe = new ImageIcon("imagenes"+File.separator+"cafe.jpg");
	private ImageIcon imgLicor = new ImageIcon("imagenes"+File.separator+"licor.jpg");
	
	JTable tablaInforme = new JTable();
	
	public Inicio(Usuario user) {
		setSize(1100, 700);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		lUser = user.getNombre() + " " + user.getApellidos();
		
		crearArticulos();
		
		//Container cp = getContentPane();
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTabbedPane tabs = new JTabbedPane();
		
		JPanel vinos = new JPanel();
		JPanel refrescos = new JPanel();
		JPanel cafes = new JPanel();
		JPanel licores = new JPanel();
		
		JPanel artVinos = new JPanel();
		JPanel artRefrescos = new JPanel();
		JPanel artCafes = new JPanel();
		JPanel artLicores = new JPanel();
		
		JScrollPane scrollVinos = new JScrollPane(vinos);
		JScrollPane scrollRefrescos = new JScrollPane(refrescos);
		JScrollPane scrollCafes = new JScrollPane(cafes);
		JScrollPane scrollLicores = new JScrollPane(licores);
		
		for(Articulo a : articulos) {
			if(a.getCategoria().equalsIgnoreCase("Vinos"))
				uv++;
				
			if(a.getCategoria().equalsIgnoreCase("Refrescos"))
				ur++;
			
			if(a.getCategoria().equalsIgnoreCase("Cafes"))
				uc++;
			
			if(a.getCategoria().equalsIgnoreCase("Licores"))
				ul++;
		}
		
		GridLayout glv = new GridLayout(uv/3, 3);
		glv.setVgap(10);
		glv.setHgap(10);
		artVinos.setLayout(glv);
		
		GridLayout glr = new GridLayout(ur/3, 3);
		glr.setVgap(10);
		glr.setHgap(10);
		artRefrescos.setLayout(glr);
		
		GridLayout glc = new GridLayout(uc/3, 3);
		glc.setVgap(10);
		glc.setHgap(10);
		artCafes.setLayout(glc);
		
		GridLayout gll = new GridLayout(ul/3, 3);
		gll.setVgap(10);
		gll.setHgap(10);
		artLicores.setLayout(gll);
		
		vinos.add(artVinos);
		refrescos.add(artRefrescos);
		cafes.add(artCafes);
		licores.add(artLicores);
		
		Image imagen1 = imgVino.getImage();
		imagen1 = imagen1.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		//imgVino = new ImageIcon(imagen1);
		imgVino.setImage(imagen1);
		
		imagen1 = imgRefresco.getImage();
		imagen1 = imagen1.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		imgRefresco.setImage(imagen1);
		
		imagen1 = imgCafe.getImage();
		imagen1 = imagen1.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		imgCafe.setImage(imagen1);
		
		imagen1 = imgLicor.getImage();
		imagen1 = imagen1.getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		imgLicor.setImage(imagen1);
		
		tabs.addTab("Vinos", imgVino, scrollVinos);
		tabs.addTab("Refrescos", imgRefresco, scrollRefrescos);
		tabs.addTab("Caf�s", imgCafe, scrollCafes);
		tabs.addTab("Licores", imgLicor, scrollLicores);
		tabs.setTitleAt(0, "Vinos");
		tabs.setTitleAt(1, "Refrescos");
		tabs.setTitleAt(2, "Caf�s");
		tabs.setTitleAt(3, "Licores");
		
		for(Articulo a : articulos) {
			JButton boton = new JButton(a.getNombre());
			
			boton.setFont(new Font("Dialog", 0, 18));
			boton.setPreferredSize(new Dimension(150, 80));
			
			if(a.getCategoria().equalsIgnoreCase("Vinos"))
				artVinos.add(boton);
			
			if(a.getCategoria().equalsIgnoreCase("Refrescos"))
				artRefrescos.add(boton);
			
			if(a.getCategoria().equalsIgnoreCase("Cafes"))
				artCafes.add(boton);
			
			if(a.getCategoria().equalsIgnoreCase("Licores"))
				artLicores.add(boton);
			
			boton.addActionListener(new A�adir(a.getNombre(), a.getPrecio()));
		}
		
		scrollCuenta.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollCuenta.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		
		tablaCuenta = new JTable();
		tablaCuenta.setEnabled(false);
		tablaCuenta.setBorder(new LineBorder(Color.BLACK, 1, true));
		tablaCuenta.setModel(modelo);
		modelo.setColumnIdentifiers(columnas);
		modelo.addRow(columnas);
		panelCuenta.add(tablaCuenta);
		tablaCuenta.setRowHeight(20);
		
		
		tablaInforme.setVisible(true);
		panelCuenta.add(tablaInforme);
		
		
		tablaCuenta.addMouseListener(new java.awt.event.MouseAdapter() {
		    @Override
		    public void mouseClicked(java.awt.event.MouseEvent evt) {
		        int row = tablaCuenta.rowAtPoint(evt.getPoint());
		        int col = tablaCuenta.columnAtPoint(evt.getPoint());
		        
		        total = 0.00;
		        
		        
		        if(col == 3 && row != 0) {
		        	modelo.removeRow(row);
		        	cesta.remove(row-1);
		        	
		        	if(cesta.size() != 0) 
			        	for(Cesta art : cesta) {
			        		total += art.getPrecio();
			        	}
		        	
		        }
		        
		        lTotal.setText("Total: " + total + " �");
		        
		    }
		});
		
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tablaCuenta.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(3).setCellRenderer( centerRenderer );
		
		JPanel panelUsuario = new JPanel();
		String nombreUsuario;
		panelUsuario.setLayout(new BorderLayout(0, 0));
		JLabel us = new JLabel("Usuario: " + lUser);
		us.setHorizontalAlignment(SwingConstants.CENTER);
		us.setFont(new Font("Dialog", Font.PLAIN, 24));
		panelUsuario.add(us, BorderLayout.WEST);
		
		cp.add(tabs);
		cp.add(scrollCuenta, BorderLayout.EAST);
		cp.add(panelUsuario, BorderLayout.SOUTH);
		
		JPanel panelTotal = new JPanel();
		panelUsuario.add(panelTotal, BorderLayout.EAST);
		
		GridLayout grid = new GridLayout(1, 2);
		grid.setHgap(25);
		
		panelTotal.setLayout(grid);
		
		JButton informe = new JButton("Informe");
		informe.setAlignmentX(Component.CENTER_ALIGNMENT);
		informe.setFont(new Font("Dialog", 0, 14));
		informe.setPreferredSize(new Dimension(120, 45));
		informe.addActionListener(new Informe());
		
		JButton gastos = new JButton("Gastos mes");
		gastos.setAlignmentX(Component.CENTER_ALIGNMENT);
		gastos.setFont(new Font("Dialog", 0, 14));
		gastos.setPreferredSize(new Dimension(120, 45));
		gastos.addActionListener(new GastosMess(user));
		
		JButton mesas = new JButton("Mesas");
		mesas.setAlignmentX(Component.CENTER_ALIGNMENT);
		mesas.setFont(new Font("Dialog", 0, 14));
		mesas.setPreferredSize(new Dimension(80, 45));
		mesas.addActionListener(new MostrarMesas(user));
				
		JButton bCuenta = new JButton("Cuenta");
		bCuenta.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelTotal.add(informe);
		panelTotal.add(gastos);
		panelTotal.add(mesas);
		panelTotal.add(bCuenta);
		bCuenta.addActionListener(new CalcularCuenta(user));
		
		bCuenta.setFont(new Font("Dialog", 0, 14));
		bCuenta.setPreferredSize(new Dimension(80, 45));
	
		lTotal.setFont(new Font("Dialog", 0, 20));
		lTotal.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panelTotal.add(lTotal);
		
	}

	public void crearArticulos() {
		articulos = ModArticulo.selecArticulos();
	}
	
	class A�adir implements ActionListener {
		private String nombre;
		private Double precio;
		
 		public A�adir(String nombre, double precio) {
			this.nombre = nombre;
			this.precio = precio;
		}

		public void actionPerformed(ActionEvent e) {
 			JButton b = (JButton) e.getSource();
 			boolean encontrado = false;
 			int fila = 0;
 			
 			if(modelo.getRowCount()>0){
	 			for (int i = 0; i < modelo.getRowCount(); i++) {
	                
	                String valor = (String) modelo.getValueAt(i, 1);
	                
	                if(b.getText().equalsIgnoreCase(valor)) {
	                	encontrado = true;
	                	fila = i;
	                	break;
	                }
	                else {
	                	encontrado = false;
	                }
	 			}
 			}
 			
 			if(encontrado) {
            	int u = Integer.parseInt((String) modelo.getValueAt(fila, 0));
            	int nuevasU = u+1;
            	modelo.setValueAt(String.valueOf(nuevasU), fila, 0);
            	
            	modelo.setValueAt(String.valueOf(precio*nuevasU), fila, 2);
            	
            	for(Cesta art : cesta) {
            		if(art.getNomArt().equalsIgnoreCase((String) modelo.getValueAt(fila, 1))) {
            			art.setUnidades(nuevasU);
            			art.setPrecio(precio*nuevasU);
            		}
            	}
            }
            else {
            	Cesta art = new Cesta(nombre, 1, precio);
				cesta.add(art);
				imprimirCuenta(art);
            }
 			
 			total = 0.0;
 			for(Cesta c : cesta) {
 				total += c.getPrecio();
 			}
 			
 			lTotal.setText("Total: " + total + " �");
 		}
 		
 		public void imprimirCuenta(Cesta art) {
 			
			String[] datos = {String.valueOf(art.getUnidades()), art.getNomArt(), String.valueOf(art.getPrecio()), "X"};
			modelo.addRow(datos);	
 		}
	}
	
	class CalcularCuenta implements ActionListener {
		
		Usuario user;
		
		public CalcularCuenta(Usuario user) {
			this.user = user;
		}
		
 		public void actionPerformed(ActionEvent e) {
 			if(cesta.size() == 0)
 				JOptionPane.showMessageDialog(null, "No hay art�culos en la cuenta");
 			else {
 				VentanaCuenta v = new VentanaCuenta(cesta, user, getContentPane());
	 			v.setVisible(true);
	 			v.setLocationRelativeTo(null);
 			}
 			
 		}
	}
	
	class MostrarMesas implements ActionListener {
		
		Usuario user;
		
		public MostrarMesas(Usuario user) {
			this.user = user;
		}
		
 		public void actionPerformed(ActionEvent e) {
 			 VentanaMesas v = new VentanaMesas(user);
 			 v.setVisible(true);
 		}
	}
	
	class GastosMess implements ActionListener {
		
		Usuario user;
		
		public GastosMess(Usuario user) {
			this.user = user;
		}
		
 		public void actionPerformed(ActionEvent e) {
 			GastosMes g = new GastosMes(user);
 			g.setVisible(true);
 		}
	}
	
	class Informe implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			int cont = 0;
 			
 			tablaInforme.setBorder(new LineBorder(Color.BLACK, 1, true));
 			
 			String[] columnas2 = {"ID", "Art�culo", "Uds. disp.", "Precio", "Minimo uds."};
 			DefaultTableModel modelo2 = new DefaultTableModel(null, columnas2);
 			tablaInforme.setModel(modelo2);
 			
 			modelo2.addRow(columnas2);
 			cont ++;
 			
 			articulos2 = ModArticulo.selecArticulosInforme();
 			
 			for(Articulo a: articulos2) {
 				String[] datos = {String.valueOf(a.getId()), a.getNombre(), String.valueOf(a.getUnidades()), String.valueOf(a.getPrecio()), String.valueOf(a.getMinUnidades())};
 				modelo2.addRow(datos);
 				cont++;
 				
 				/*if(a.getUnidades() <= a.getMinUnidades()) {
 					System.out.println(a.getUnidades() + " / " + a.getMinUnidades());
 					DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
 					centerRenderer.setForeground(Color.RED);
 					
 					//cambiar color de la row no del column
 					tablaInforme.getModel().getValueAt(cont, 0);
 					tablaInforme.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
 					tablaInforme.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
 					tablaInforme.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
 					tablaInforme.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
 					tablaInforme.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
 				}*/
 					
 				
 			}
 			
 			try {

 				boolean imp = tablaInforme.print();
 				
 				if(!imp)
 					JOptionPane.showMessageDialog(null, "No se puede imprimir la cuenta.");
 				else
 					JOptionPane.showMessageDialog(null, "Informe impreso con �xito.");
 			
 				tablaInforme.setVisible(false);
 				
 			} catch (PrinterException e2) {
 				JOptionPane.showMessageDialog(null, e2.getMessage());
 			}
 			
 			//VentanaInforme v = new VentanaInforme(tablaInforme);
 			//v.setVisible(true);
 			
 		}
	}
	
	
}
