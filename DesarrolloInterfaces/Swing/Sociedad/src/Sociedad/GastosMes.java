package Sociedad;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import com.toedter.calendar.JCalendar;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import Clases.Cesta;
import Clases.Factura;
import Clases.Mesa;
import Clases.Reserva;
import Clases.Usuario;
import Modelo.ModFactura;
import Modelo.ModMesa;
import Modelo.ModReserva;

import javax.swing.JComboBox;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

public class GastosMes extends JFrame {
	
	SimpleDateFormat formateador = new SimpleDateFormat("yyyy/MM/dd");
	
	private JPanel panelFechas = new JPanel();
	private JPanel panelFacturas = new JPanel();
	private JPanel panelTotal = new JPanel();
	
	private JDateChooser fecha1 = new JDateChooser();
	private JDateChooser fecha2 = new JDateChooser();
	
	private Double total = 0.0;
	private Usuario user;
	
	private JTable tablaCuenta = new JTable();
	private String[] columnas = {"ID", "FECHA", "TOTAL (�)"};
	private DefaultTableModel modelo = new DefaultTableModel(null, columnas);
	
	public GastosMes(Usuario user) {
		this.user = user;
		setSize(800, 500);
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setLocationRelativeTo(null);
		
		Container cp = getContentPane();
		cp.setLayout(new GridLayout(3, 1));
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.add(Calendar.MONTH, -1);
		fecha1.setDate(calendar.getTime());
		fecha2.setDate(new Date());
		
		panelFechas.add(new JLabel("Inicio: "));		
		panelFechas.add(fecha1);		
		panelFechas.add(new JLabel("      Fin: "));		
		panelFechas.add(fecha2);
		
		tablaCuenta.setEnabled(false);
		
		JScrollPane scrollCuenta = new JScrollPane(tablaCuenta);
		tablaCuenta.setBorder(new LineBorder(Color.BLACK, 1, true));
		tablaCuenta.setModel(modelo);
		modelo.setColumnIdentifiers(columnas);
		modelo.addRow(columnas);
		
		tablaCuenta.setRowHeight(25);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tablaCuenta.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		selecFacturas();
		
		panelFacturas.add(tablaCuenta);
		panelTotal.add(new JLabel("TOTAL: " + total + "�"));
		
		cp.add(panelFechas);		
		cp.add(panelFacturas);	
		cp.add(panelTotal);	
		
	}
	
	public void selecFacturas() {
		try {
			List<Factura> facturas = ModFactura.selecFacturas(formateador.format(fecha1.getDate()), formateador.format(fecha2.getDate()), user);
			
			if(facturas != null) {
				for(Factura f: facturas) {
					double precio = 0.0;
					
					for(Cesta c: f.getArticulos()) {
						precio += c.getPrecio();
					}
				
					
					String[] fac = {String.valueOf(f.getId()), f.getFecha().toString(), String.valueOf(precio)};
					modelo.addRow(fac);
					total += precio;
				}
			}
				
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
