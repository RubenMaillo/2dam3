package Sociedad;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.Document;

import Clases.Cesta;
import Clases.Usuario;
import Modelo.ModCesta;
import Modelo.ModFactura;


public class VentanaCuenta extends JFrame{
	
	private static DecimalFormat df2 = new DecimalFormat("#.##");
	
	public VentanaCuenta(ArrayList<Cesta> cesta, Usuario user, Container container) {
		setSize(900, 600);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		Container cp = getContentPane();
		getContentPane().setLayout(new BorderLayout(0, 0));
		
		JTable tablaCuenta = new JTable();
		
		
		JPanel panelOpciones = new JPanel();
		GridLayout gl = new GridLayout(1, 4);
		gl.setHgap(100);
		
		JButton pagar = new JButton("Pagar");
		JButton salir = new JButton("Salir");
		JButton calcular = new JButton("Calcular");
		
		pagar.setFont(new Font("Dialog", 0, 14));
		pagar.setPreferredSize(new Dimension(100, 45));
		
		salir.setFont(new Font("Dialog", 0, 14));
		salir.setPreferredSize(new Dimension(100, 45));
		
		calcular.setFont(new Font("Dialog", 0, 14));
		calcular.setPreferredSize(new Dimension(100, 45));
		
		pagar.addActionListener(new Pagar(cesta, tablaCuenta, container, user));
		salir.addActionListener(new Salir());
		
		panelOpciones.add(pagar);
		panelOpciones.add(salir);
		
		Double total= 0.00;
		
		tablaCuenta.setEnabled(false);
		String[] columnas = {"Cant.", "Artículo", "Precio"};
		DefaultTableModel modelo = new DefaultTableModel(null, columnas);
		JScrollPane scrollCuenta = new JScrollPane(tablaCuenta);
		tablaCuenta.setBorder(new LineBorder(Color.BLACK, 1, true));
		tablaCuenta.setModel(modelo);
		modelo.setColumnIdentifiers(columnas);
		
		tablaCuenta.setRowHeight(25);
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment( JLabel.CENTER );
		tablaCuenta.getColumnModel().getColumn(0).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(1).setCellRenderer( centerRenderer );
		tablaCuenta.getColumnModel().getColumn(2).setCellRenderer( centerRenderer );
		
		for(Cesta c : cesta) {
			String[] datos = {String.valueOf(c.getUnidades()), c.getNomArt(), String.valueOf(c.getPrecio())};
			total += c.getPrecio();
			modelo.addRow(datos);
		}
		
		cp.add(panelOpciones, BorderLayout.NORTH);
		cp.add(scrollCuenta, BorderLayout.CENTER);
		
		
		JPanel panelUsuario = new JPanel();
		panelUsuario.setLayout(new BorderLayout(0, 0));
		JLabel us = new JLabel("Usuario: " + user.getNombre() + " " + user.getApellidos());
		us.setFont(new Font("Dialog", Font.PLAIN, 20));
		panelUsuario.add(us, BorderLayout.WEST);
		
		JLabel precio = new JLabel("Total: " + String.valueOf(total) + " €");
		precio.setFont(new Font("Dialog", Font.PLAIN, 20));
		panelUsuario.add(precio, BorderLayout.EAST);
		
		cp.add(panelUsuario, BorderLayout.SOUTH);
	}
	
	class Pagar implements ActionListener {
		
		ArrayList<Cesta> cesta;
		JTable tablaCuenta;
		Container container;
		Usuario user;
		
		public Pagar(ArrayList<Cesta> cesta, JTable tablaCuenta, Container container, Usuario user) {
			this.cesta = cesta;
			this.tablaCuenta = tablaCuenta;
			this.container = container;
			this.user = user;
		}
		
 		public void actionPerformed(ActionEvent e) {
 			int imprimir = JOptionPane.showConfirmDialog(null, "¿Desea imprimir la cuenta a pdf?", "Imprimir cuenta",JOptionPane.YES_NO_OPTION);
 			if(imprimir == 0) {
 				imprimirPDF();
 			}
 			
 			ModFactura.addFactura(user);
 			ModCesta.addCuenta(cesta);
			JOptionPane.showMessageDialog(null, "Pago realizado");
			cesta = null;
			JFrame topFrame = (JFrame) SwingUtilities.getWindowAncestor(container);
 			topFrame.dispose();
			dispose();
			Sociedad.main(null);
 		}
 		
 		public void imprimirPDF() {
 			try {

				boolean imp = tablaCuenta.print();
				
				if(!imp)
					JOptionPane.showMessageDialog(null, "No se puede imprimir la cuenta.");
				
			} catch (PrinterException e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
			}
 		}
	}
	
	class Salir implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			dispose();
 		}
	}
}
