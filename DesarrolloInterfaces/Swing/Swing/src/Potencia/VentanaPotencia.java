package Potencia;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class VentanaPotencia extends JFrame{
	
	private JTextField base, potencia;
	private JButton limpiar, calcular, salir, mr1, ms, mr, mm;
	private JLabel lblBase, lblPotencia, lblRes, txtRes;

	public VentanaPotencia() {
		super("Ventana 1");
		setSize(250, 150);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		
		base = new JTextField(5);
		potencia = new JTextField(5);
		
		limpiar = new JButton("Limpiar");
		calcular = new JButton("Calcular");
		salir = new JButton("Salir");
		
		lblBase = new JLabel("Base");
		lblPotencia = new JLabel("Potencia");
		lblRes = new JLabel("Resultado");
		txtRes = new JLabel("---");
		
		JPanel panelDatos = new JPanel();
		GridLayout gl = new GridLayout(3,2);
		gl.setHgap(30);
		gl.setVgap(30);
		panelDatos.setLayout(gl);
		
		panelDatos.add(lblBase);
		panelDatos.add(base);
		panelDatos.add(lblPotencia);
		panelDatos.add(potencia);
		panelDatos.add(lblRes);
		panelDatos.add(txtRes);
		
		JPanel panelBotones = new JPanel();
		GridLayout gl1 = new GridLayout(3,2);
		gl1.setHgap(5);
		gl1.setVgap(5);
		panelDatos.setLayout(gl1);
		
		limpiar.addActionListener(new Limpiar());
		calcular.addActionListener(new Calcular());
		salir.addActionListener(new Salir());
		
		panelBotones.add(limpiar);
		panelBotones.add(calcular);
		panelBotones.add(salir);
		
		cp.add(panelDatos);
		cp.add(panelBotones);
	}
	
	class Limpiar implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			base.setText(null);
			potencia.setText(null);
			txtRes.setText("---");
		}
	}
	
	class Calcular implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			int res = 1;
			try {
				int nbase = Integer.parseInt(base.getText());
				int npotencia = Integer.parseInt(potencia.getText());
				
				for(int i=0; i<npotencia; i++) {
					res *= nbase;
				}
				
				txtRes.setText(String.valueOf(res));
			}catch(Exception err) {
				JOptionPane.showMessageDialog(null, "Error, vuelva a ingresar los datos.");
			}
			
			 
		}
	}
	
	class Salir implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}
	 
}
