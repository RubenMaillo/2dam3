package Navegacion;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Ventana1 extends JFrame {
	
	class EventoSaludo implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			dispose();
 			Ventana2 v2 = new Ventana2();
 			v2.setVisible(true);
 			v2.setLocationRelativeTo(null);
 		}
	 }
	
	public Ventana1() {
		super("Ventana 1");
		setSize(250, 125);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		
		JButton boton = new JButton("Siguiente");
		boton.addActionListener(new EventoSaludo());
		
		cp.add(boton);
	 }
}