import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class BotonVentana extends JFrame implements ActionListener{
	public BotonVentana() {
		super("Bot�n");
		setSize(200,100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		JButton boton = new JButton("�P�lsame!");
		boton.addActionListener(this);
		cp.add(boton);
	}

	 public void actionPerformed(ActionEvent e) {
		 JButton boton = (JButton) e.getSource();
		 boton.setText("�Gracias!");
	 }

}