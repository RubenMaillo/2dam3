package Calculadora;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

public class VentanaCalculadora extends JFrame{
	
	private JTextField texto;
	private JButton n0, n1, n2, n3, n4, n5, n6, n7, n8, n9, bM, bD, bS, bR, bC, bI;
	private String num1, num2, operador;
	
	public VentanaCalculadora(){
		super("Calculadora");
		setSize(250, 200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		
		texto = new JTextField(18);
		texto.setEnabled(false);
		
		JPanel panelNum = new JPanel();
		GridLayout gl = new GridLayout(4,4);
		gl.setHgap(5);
		gl.setVgap(5);
		panelNum.setLayout(gl);
		
		n0 = new JButton("0"); n1 = new JButton("1"); n2 = new JButton("2");
		n3 = new JButton("3"); n4 = new JButton("4"); n5 = new JButton("5");
		n6 = new JButton("6"); n7 = new JButton("7"); n8 = new JButton("8");
		n9 = new JButton("9"); bM = new JButton("*"); bD = new JButton("/");
		bS = new JButton("+"); bR = new JButton("-"); bC = new JButton(",");
		bI = new JButton("=");
		
		num1 = "";
		num2 = "";
		operador = "";
		
		JButton[] botones = new JButton[] {
            n7, n8, n9, bM, n4, n5, n6, bD, n1, n2, n3, bS, n0, bC, bI, bR
        };
		
		for(JButton boton : botones) {
			panelNum.add(boton);
			boton.addActionListener(new Calcular());
		}
		
		cp.add(texto);
		cp.add(panelNum);
		
	}
	
	
	
	class Calcular implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			JButton b = (JButton) e.getSource();
 			int res = 0;
 			
 			if(b.getText().equalsIgnoreCase("=")) {
 				
 				
 				if(num2.substring(0, 1).equals("+")){
 					res = Integer.parseInt(num1) + Integer.parseInt(num2.substring(1, num2.length()));
 					
 				}
 				
 				if(num2.substring(0, 1).equals("-")){
 					res = Integer.parseInt(num1) - Integer.parseInt(num2.substring(1, num2.length()));
 				}
 				
 				if(num2.substring(0, 1).equals("*")){
 					res = Integer.parseInt(num1) * Integer.parseInt(num2.substring(1, num2.length()));
 				}
 				
 				if(num2.substring(0, 1).equals("/")){
 					res = Integer.parseInt(num1) / Integer.parseInt(num2.substring(1, num2.length()));
 				}
 				
 				texto.setText(String.valueOf(res));
 				num1 = "";
 				num2 = "";
 				operador = "";
 			}
 			else {
 			
	 			if(b.getText().equalsIgnoreCase("+") || b.getText().equalsIgnoreCase("-") || b.getText().equalsIgnoreCase("*") || b.getText().equalsIgnoreCase("/")) {
	 				operador = b.getText();
	 				texto.setText(operador);
	 			}
	 			
	 			if(operador == "") {
	 				num1 += b.getText();
	 				texto.setText(num1);
	 			}
	 			else {
	 				num2 += b.getText();
	 				texto.setText(num2.substring(1, num2.length()));
	 				operador = num2.substring(0);
	 			}
 			}

 		}
	 }

}
