import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class MiVentana extends JFrame {
	
	private JTextField cuadroTexto;
	
 	class EventoSaludo implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			JOptionPane.showMessageDialog(null, "�Hola, " + cuadroTexto.getText() + "!");
 		}
	 }
	 
	public MiVentana() {
		super("Titulo de ventana");
		setSize(400, 300);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		JLabel etiqueta = new JLabel("Nombre: ");
		cuadroTexto = new JTextField(20);
		cp.add(etiqueta);
		cp.add(cuadroTexto);
		
		
		JButton botonSaludo = new JButton("Saludar");
		cp.add(botonSaludo);
		botonSaludo.addActionListener(new EventoSaludo());

	}
}
