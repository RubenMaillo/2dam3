package Ejercicios;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class VentanaSaludo extends JFrame{
	
	private JTextField cuadroTexto;
	
	class EventoHola implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			cuadroTexto.setText("Hola");
		}
	}
	
	class EventoAdios implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			cuadroTexto.setText("Adios");
		}
	}
	
	public VentanaSaludo() {
		super("Saludo");
		setSize(250, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		
		JButton botonHola = new JButton("Di Hola");
		JButton botonAdios = new JButton("Di Adios");
		cuadroTexto = new JTextField(20);
		
		cp.add(botonHola);
		cp.add(botonAdios);
		cp.add(cuadroTexto);
		
		botonHola.addActionListener(new EventoHola());
		botonAdios.addActionListener(new EventoAdios());
	}
}
