package Ejercicios;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

public class VentanaIngresoDatos extends JFrame{
	
	private ArrayList<Persona> personas = new ArrayList<Persona>();
	private JTextField textApellido1, textApellido2, textNombre;
	private JComboBox comboEstado;
	JRadioButton hombre, mujer;
	ButtonGroup bG;
	DefaultTableModel modelo;
	JTable tabla;
	
	public VentanaIngresoDatos(){
		super("Saludo");
		setSize(550, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		Container cp = getContentPane();
		cp.setLayout(new FlowLayout());
		
		personas.add(new Persona("Pepito", "Papo", "Pepe", "Soltero", "Hombre"));
		
		
		JPanel panelDatos = new JPanel();
		TitledBorder borde1 = new TitledBorder("Datos personales");
	    borde1.setTitleJustification(TitledBorder.CENTER);
	    borde1.setTitlePosition(TitledBorder.TOP);
		panelDatos.setBorder(borde1); 
		GridLayout gl = new GridLayout(5,2);
		gl.setHgap(5);
		gl.setVgap(5);
		panelDatos.setLayout(gl);
		
		JLabel lblApellido1 = new JLabel("Apellido paterno: ");
		textApellido1 = new JTextField(20);
		
		JLabel lblApellido2 = new JLabel("Apellido materno: ");
		textApellido2 = new JTextField(20);
		
		JLabel lblNombre = new JLabel("Nombre: ");
		textNombre = new JTextField(20);
		
		JLabel lblEstado = new JLabel("Estado civil: ");
		comboEstado = new JComboBox();
		
		JPanel panelSexo = new JPanel();
		TitledBorder borde2 = new TitledBorder("Sexo");
	    borde2.setTitleJustification(TitledBorder.CENTER);
	    borde2.setTitlePosition(TitledBorder.TOP);
		panelSexo.setBorder(borde2); 
		
		hombre = new JRadioButton("Hombre");
        mujer = new JRadioButton("Mujer");
        bG= new ButtonGroup();
        bG.add(hombre);
        bG.add(mujer);
		
		comboEstado.addItem("Soltero");
		comboEstado.addItem("En relacion");
		comboEstado.addItem("Casado");
		comboEstado.addItem("Divorciado");
		comboEstado.addItem("Viudx");
		comboEstado.setSelectedIndex(0);
		
		panelSexo.add(hombre);
		panelSexo.add(mujer);
		
		panelDatos.add(lblApellido1);
		panelDatos.add(textApellido1);
		panelDatos.add(lblApellido2);
		panelDatos.add(textApellido2);
		panelDatos.add(lblNombre);
		panelDatos.add(textNombre);
		panelDatos.add(lblEstado);
		panelDatos.add(comboEstado);
		panelDatos.add(panelSexo);
		
		
		//------------------------------------------
		JPanel panelBotones = new JPanel();
		GridLayout gl2 = new GridLayout(1,2);
		gl2.setHgap(100);
		panelBotones.setLayout(gl2);
		
		JButton guardar = new JButton("Guardar");
		JButton salir = new JButton("Salir");
		
		guardar.addActionListener(new EventoGuardar());
		salir.addActionListener(new salir());
		
		panelBotones.add(guardar);
		panelBotones.add(salir);
		
		//------------------------------------------
		JPanel panelLista = new JPanel();
		TitledBorder borde3 = new TitledBorder("Listado");
	    borde3.setTitleJustification(TitledBorder.CENTER);
	    borde3.setTitlePosition(TitledBorder.TOP);
		panelSexo.setBorder(borde3); 
		
		modelo = new DefaultTableModel(null, new String[] {"Apellido paterno", "Apellido materno", "Nombre", "Estado civil", "Sexo"});
		
		Object o[] = null;
		for(int i = 0; i<personas.size(); i++) {
			modelo.addRow(o);
			Persona p = (Persona) personas.get(i);
			modelo.setValueAt(p.getApellidoPaterno(), i, 0);
			modelo.setValueAt(p.getApellidoMaterno(), i, 1);
			modelo.setValueAt(p.getNombre(), i, 2);
			modelo.setValueAt(p.getEstado(), i, 3);
			modelo.setValueAt(p.getSexo(), i, 4);
		}
		modelo.fireTableDataChanged();
		tabla = new JTable(modelo);
		
		panelLista.add(new JScrollPane(tabla));
		
		cp.add(panelDatos);
		cp.add(panelBotones);
		cp.add(panelLista);
	}
	
	class EventoGuardar implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			//System.out.println("prueba");
 			String estado = comboEstado.getSelectedItem().toString();
 			String sexo = "";
 			
 			for(Enumeration<AbstractButton> b = bG.getElements(); b.hasMoreElements();) {
 				AbstractButton bu = b.nextElement();
 				
 				if(bu.isSelected()) {
 					sexo = bu.getText();
 				}
 			}
 			
 			personas.add(new Persona(textApellido1.getText().toString(), textApellido2.getText().toString(), textNombre.getText().toString(), estado, sexo));

 			DefaultTableModel m = (DefaultTableModel) tabla.getModel();
 			m.fireTableDataChanged();
 			
 		}
	 }
	
	class salir implements ActionListener {
 		public void actionPerformed(ActionEvent e) {
 			System.exit(0);
 		}
	 }
}
